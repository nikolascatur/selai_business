package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class CreateSaleModel implements Parcelable {
    @SerializedName("id")
    private int id;

    @SerializedName("merchant_id")
    private int merchant_id;

    @SerializedName("started_at")
    private String started_at;

    @SerializedName("durations")
    private int durations;

    @SerializedName("description")
    private String description;

    @SerializedName("catalog_url")
    private String catalog_url;

    @SerializedName("catalog_username")
    private String catalog_username;

    @SerializedName("maximum_buy")
    private int maximum_buy;

    private List<CourierModel> couriers;

    @SerializedName("total_product")
    private int total_product;

    @SerializedName("is_active")
    private boolean is_active;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    protected CreateSaleModel(Parcel in) {
        id = in.readInt();
        merchant_id = in.readInt();
        started_at = in.readString();
        durations = in.readInt();
        description = in.readString();
        catalog_url = in.readString();
        catalog_username = in.readString();
        maximum_buy = in.readInt();
        couriers = in.createTypedArrayList(CourierModel.CREATOR);
        total_product = in.readInt();
        is_active = in.readByte() != 0;
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(merchant_id);
        dest.writeString(started_at);
        dest.writeInt(durations);
        dest.writeString(description);
        dest.writeString(catalog_url);
        dest.writeString(catalog_username);
        dest.writeInt(maximum_buy);
        dest.writeTypedList(couriers);
        dest.writeInt(total_product);
        dest.writeByte((byte) (is_active ? 1 : 0));
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CreateSaleModel> CREATOR = new Creator<CreateSaleModel>() {
        @Override
        public CreateSaleModel createFromParcel(Parcel in) {
            return new CreateSaleModel(in);
        }

        @Override
        public CreateSaleModel[] newArray(int size) {
            return new CreateSaleModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getStarted_at() {
        return started_at;
    }

    public void setStarted_at(String started_at) {
        this.started_at = started_at;
    }

    public int getDurations() {
        return durations;
    }

    public void setDurations(int durations) {
        this.durations = durations;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCatalog_url() {
        return catalog_url;
    }

    public void setCatalog_url(String catalog_url) {
        this.catalog_url = catalog_url;
    }

    public String getCatalog_username() {
        return catalog_username;
    }

    public void setCatalog_username(String catalog_username) {
        this.catalog_username = catalog_username;
    }

    public int getMaximum_buy() {
        return maximum_buy;
    }

    public void setMaximum_buy(int maximum_buy) {
        this.maximum_buy = maximum_buy;
    }

    public List<CourierModel> getCouriers() {
        return couriers;
    }

    public void setCouriers(List<CourierModel> couriers) {
        this.couriers = couriers;
    }

    public int getTotal_product() {
        return total_product;
    }

    public void setTotal_product(int total_product) {
        this.total_product = total_product;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

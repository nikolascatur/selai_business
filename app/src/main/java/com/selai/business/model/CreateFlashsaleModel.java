package com.selai.business.model;

import java.util.List;

public class CreateFlashsaleModel {

    private String started_at;

    private int durations;

    private String description;

    private String catalog_url;

    private String catalog_username;

    private int maximum_buy;

    private List<Integer> couriers;

    private Boolean is_active;

    public CreateFlashsaleModel(
        String started_at,
        int durations,
        String description,
        String catalog_url,
        String catalog_username,
        int maximum_buy,
        List<Integer> couriers,
        Boolean is_active) {
        this.started_at = started_at;
        this.durations = durations;
        this.description = description;
        this.catalog_url = catalog_url;
        this.catalog_username = catalog_username;
        this.maximum_buy = maximum_buy;
        this.couriers = couriers;
        this.is_active = is_active;

    }

    public String getStarted_at() {
        return started_at;
    }

    public int getDurations() {
        return durations;
    }

    public String getDescription() {
        return description;
    }

    public String getCatalog_url() {
        return catalog_url;
    }

    public String getCatalog_username() {
        return catalog_username;
    }

    public int getMaximum_buy() {
        return maximum_buy;
    }

    public List<Integer> getCouriers() {
        return couriers;
    }

    public Boolean getIs_active() {
        return is_active;
    }
}

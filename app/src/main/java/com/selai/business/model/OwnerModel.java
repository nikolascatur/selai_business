package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class OwnerModel implements Parcelable{
    @SerializedName("id")
    private int id;
    @SerializedName("merchant_id")
    private int merchant_id;
    @SerializedName("owner_image")
    private String owner_image;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("id_number")
    private String id_number;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("updated_at")
    private String updated_at;

    protected OwnerModel(Parcel in) {
        id = in.readInt();
        merchant_id = in.readInt();
        owner_image = in.readString();
        name = in.readString();
        email = in.readString();
        id_number = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<OwnerModel> CREATOR = new Creator<OwnerModel>() {
        @Override
        public OwnerModel createFromParcel(Parcel in) {
            return new OwnerModel(in);
        }

        @Override
        public OwnerModel[] newArray(int size) {
            return new OwnerModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getOwner_image() {
        return owner_image;
    }

    public void setOwner_image(String owner_image) {
        this.owner_image = owner_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(merchant_id);
        dest.writeString(owner_image);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(id_number);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }
}

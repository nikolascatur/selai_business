package com.selai.business.model;

public class SaleStatistic {

    public TYPE_BUTTON getType() {
        return type;
    }

    public void setType(TYPE_BUTTON type) {
        this.type = type;
    }

    public enum TYPE_BUTTON {
        JUST_TEXT,
        REGULAR,
        ICON
    }

    private String total;
    private String description;
    private TYPE_BUTTON type;

    public SaleStatistic(String total,String description,TYPE_BUTTON type){
        this.total = total;
        this.description = description;
        this.type = type;
    }
    public String getTotal() {
        return total;
    }

    public String getDescription() {
        return description;
    }
}

package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

public class Variant {

    @SerializedName("id")
    private int id;

    @SerializedName("flash_sale_product_id")
    private int flash_sale_product_id;

    @SerializedName("variant")
    private String variant;

    @SerializedName("stock")
    private int stock;

    @SerializedName("stock_outgoing")
    private int stock_outgoing;

    @SerializedName("weight")
    private int weight;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlash_sale_product_id() {
        return flash_sale_product_id;
    }

    public void setFlash_sale_product_id(int flash_sale_product_id) {
        this.flash_sale_product_id = flash_sale_product_id;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStock_outgoing() {
        return stock_outgoing;
    }

    public void setStock_outgoing(int stock_outgoing) {
        this.stock_outgoing = stock_outgoing;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}

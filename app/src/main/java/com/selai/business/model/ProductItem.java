package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductItem {

    @SerializedName("id")
    private int id;

    @SerializedName("flash_sale_id")
    private int flash_sale_id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @SerializedName("price")
    private int price;

    @SerializedName("discount")
    private int discount;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    private List<Variant> variants;

    public int getId() {
        return id;
    }

    public int getFlash_sale_id() {
        return flash_sale_id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public int getPrice() {
        return price;
    }

    public int getDiscount() {
        return discount;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public List<Variant> getVariants() {
        return variants;
    }

}

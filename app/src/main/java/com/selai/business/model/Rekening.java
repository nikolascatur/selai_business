package com.selai.business.model;

public class Rekening {

    public String getBank() {
        return bank;
    }

    public String getNameRek() {
        return nameRek;
    }

    public String getNoRek() {
        return noRek;
    }

    public TYPE_REKENING getType() {
        return type;
    }

    public enum TYPE_REKENING{
        REGULAR,
        ADD
    }

    private String bank;

    private String nameRek;

    private String noRek;

    private TYPE_REKENING type;

    public Rekening(String bank,String nameRek,String noRek,TYPE_REKENING type){
        this.bank = bank;
        this.nameRek = nameRek;
        this.noRek = noRek;
        this.type = type;
    }



}

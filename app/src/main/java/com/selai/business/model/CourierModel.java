package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class CourierModel implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @SerializedName("code")
    private String code;

    @SerializedName("is_active")
    private boolean is_active;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("costs")
    private String costs;

    protected CourierModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
        code = in.readString();
        is_active = in.readByte() != 0;
        created_at = in.readString();
        updated_at = in.readString();
        costs = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeString(code);
        dest.writeByte((byte) (is_active ? 1 : 0));
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(costs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CourierModel> CREATOR = new Creator<CourierModel>() {
        @Override
        public CourierModel createFromParcel(Parcel in) {
            return new CourierModel(in);
        }

        @Override
        public CourierModel[] newArray(int size) {
            return new CourierModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getCode() {
        return code;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCosts() {
        return costs;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setCosts(String costs) {
        this.costs = costs;
    }
}

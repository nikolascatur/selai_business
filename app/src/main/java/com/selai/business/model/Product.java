package com.selai.business.model;

import java.util.List;

public class Product {

    private String image;
    private String name;
    private float price;
    private float discount;
    private List<Size> variants;

    public Product(String image,String name,float price,float discount,List<Size> variants){
        this.image = image;
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.variants = variants;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public List<Size> getVariants() {
        return variants;
    }

    public void setVariants(List<Size> variants) {
        this.variants = variants;
    }
}

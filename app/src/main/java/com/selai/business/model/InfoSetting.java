package com.selai.business.model;

public class InfoSetting {

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getInfo() {
        return info;
    }

    public String getInfo_horizontal() {
        return info_horizontal;
    }

    public TYPE_INFO_SETTING getAction() {
        return action;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setInfo_horizontal(String info_horizontal) {
        this.info_horizontal = info_horizontal;
    }

    public void setAction(TYPE_INFO_SETTING action) {
        this.action = action;
    }

    public boolean isArrShow() {
        return isArrShow;
    }

    public void setArrShow(boolean arrShow) {
        isArrShow = arrShow;
    }

    public enum TYPE_INFO_SETTING{
        PHONE,
        ADDRESS,
        PASSWORD,
        LANGUAGE,

        FACEBOOK,
        GPLUS,

        USER_VERIFICATION,
        BANK_REK,

        QUESTION,
        TNC,
        PRIVACY_POLICY
    }

    private int icon;
    private String title;
    private String info;
    private String info_horizontal;
    private TYPE_INFO_SETTING action;
    private boolean isArrShow = true;

    public InfoSetting(int icon,String title,String info,String info_horizontal,TYPE_INFO_SETTING action,boolean isArrShow){
        this.icon = icon;
        this.title = title;
        this.info = info;
        this.info_horizontal = info_horizontal;
        this.action = action;
        this.isArrShow = isArrShow;
    }
}

package com.selai.business.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TimeModel implements Parcelable {

    private String date;

    private String time;

    public TimeModel(String date,String time){
        this.date = date;
        this.time = time;
    }

    protected TimeModel(Parcel in) {
        date = in.readString();
        time = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TimeModel> CREATOR = new Creator<TimeModel>() {
        @Override
        public TimeModel createFromParcel(Parcel in) {
            return new TimeModel(in);
        }

        @Override
        public TimeModel[] newArray(int size) {
            return new TimeModel[size];
        }
    };

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }
}

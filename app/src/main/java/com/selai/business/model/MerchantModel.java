package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class MerchantModel implements Parcelable{

    @SerializedName("id")
    private int id;

    @SerializedName("image")
    private String image;

    @SerializedName("shop_name")
    private String shop_name;

    @SerializedName("address")
    private String address;

    @SerializedName("country")
    private String country;

    @SerializedName("province_id")
    private int province_id;

    @SerializedName("province_name")
    private String province_name;

    @SerializedName("city_id")
    private int city_id;

    @SerializedName("city_name")
    private String city_name;

    @SerializedName("district_id")
    private int district_id;

    @SerializedName("district_name")
    private String district_name;

    @SerializedName("village_id")
    private int village_id;

    @SerializedName("village_name")
    private String village_name;

    @SerializedName("postal_code")
    private String postal_code;

    @SerializedName("phone")
    private String phone;

    @SerializedName("notif_token")
    private String notif_token;

    @SerializedName("device_id")
    private String device_id;

    @SerializedName("is_verified")
    private boolean is_verified;

    @SerializedName("is_active")
    private boolean is_active;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;


    protected MerchantModel(Parcel in) {
        id = in.readInt();
        image = in.readString();
        shop_name = in.readString();
        address = in.readString();
        country = in.readString();
        province_id = in.readInt();
        province_name = in.readString();
        city_id = in.readInt();
        city_name = in.readString();
        district_id = in.readInt();
        district_name = in.readString();
        village_id = in.readInt();
        village_name = in.readString();
        postal_code = in.readString();
        phone = in.readString();
        notif_token = in.readString();
        device_id = in.readString();
        is_verified = in.readByte() != 0;
        is_active = in.readByte() != 0;
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<MerchantModel> CREATOR = new Creator<MerchantModel>() {
        @Override
        public MerchantModel createFromParcel(Parcel in) {
            return new MerchantModel(in);
        }

        @Override
        public MerchantModel[] newArray(int size) {
            return new MerchantModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getShop_name() {
        return shop_name;
    }

    public String getAddress() {
        return address;
    }

    public String getCountry() {
        return country;
    }

    public int getProvince_id() {
        return province_id;
    }

    public String getProvince_name() {
        return province_name;
    }

    public int getCity_id() {
        return city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public int getDistrict_id() {
        return district_id;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public int getVillage_id() {
        return village_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public void setDistrict_id(int district_id) {
        this.district_id = district_id;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public void setVillage_id(int village_id) {
        this.village_id = village_id;
    }

    public String getVillage_name() {
        return village_name;
    }

    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNotif_token() {
        return notif_token;
    }

    public void setNotif_token(String notif_token) {
        this.notif_token = notif_token;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public boolean isIs_verified() {
        return is_verified;
    }

    public void setIs_verified(boolean is_verified) {
        this.is_verified = is_verified;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(image);
        dest.writeString(shop_name);
        dest.writeString(address);
        dest.writeString(country);
        dest.writeInt(province_id);
        dest.writeString(province_name);
        dest.writeInt(city_id);
        dest.writeString(city_name);
        dest.writeInt(district_id);
        dest.writeString(district_name);
        dest.writeInt(village_id);
        dest.writeString(village_name);
        dest.writeString(postal_code);
        dest.writeString(phone);
        dest.writeString(notif_token);
        dest.writeString(device_id);
        dest.writeByte((byte) (is_verified ? 1 : 0));
        dest.writeByte((byte) (is_active ? 1 : 0));
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }
}

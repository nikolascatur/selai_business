package com.selai.business.model;

import java.util.List;

public class FlashSaleProduct {

    private String name;

    private String image;

    private int price;

    private int discount;

    private List<Variant> variants;

    public FlashSaleProduct(String name,String image,int price,int discount,List<Variant> variants){
        this.name = name;
        this.image = image;
        this.price = price;
        this.discount = discount;
        this.variants = variants;

    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public int getPrice() {
        return price;
    }

    public int getDiscount() {
        return discount;
    }

    public List<Variant> getVariants() {
        return variants;
    }
}

package com.selai.business.model;

public class Size {
    private String variant;
    private int stock;
    private int weight;

    public Size(String variant,int stock,int weight){
        this.variant = variant;
        this.stock = stock;
        this.weight = weight;
    }


    public String getVariant() {
        return variant;
    }

    public int getStock() {
        return stock;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}

package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class Province implements Parcelable{

    @SerializedName("province_id")
    private
    String province_id;

    @SerializedName("province")
    private
    String province;

    protected Province(Parcel in) {
        province_id = in.readString();
        province = in.readString();
    }

    public static final Creator<Province> CREATOR = new Creator<Province>() {
        @Override
        public Province createFromParcel(Parcel in) {
            return new Province(in);
        }

        @Override
        public Province[] newArray(int size) {
            return new Province[size];
        }
    };

    public String getProvince_id() {
        return province_id;
    }

    public String getProvince() {
        return province;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(province_id);
        dest.writeString(province);
    }
}

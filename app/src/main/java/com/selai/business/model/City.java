package com.selai.business.model;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class City implements Parcelable{

    @SerializedName("city_id")
    private
    String city_id;

    @SerializedName("province_id")
    private
    String province_id;

    @SerializedName("province")
    private
    String province;

    @SerializedName("type")
    private
    String type;

    @SerializedName("city_name")
    private
    String city_name;

    @SerializedName("postal_code")
    private
    String postal_code;

    protected City(Parcel in) {
        city_id = in.readString();
        province_id = in.readString();
        province = in.readString();
        type = in.readString();
        city_name = in.readString();
        postal_code = in.readString();
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    public String getCity_id() {
        return city_id;
    }

    public String getProvince_id() {
        return province_id;
    }

    public String getProvince() {
        return province;
    }

    public String getType() {
        return type;
    }

    public String getCity_name() {
        return city_name;
    }

    public String getPostal_code() {
        return postal_code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city_id);
        dest.writeString(province_id);
        dest.writeString(province);
        dest.writeString(type);
        dest.writeString(city_name);
        dest.writeString(postal_code);
    }
}

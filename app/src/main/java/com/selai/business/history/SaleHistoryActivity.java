package com.selai.business.history;

import com.selai.business.R;
import com.selai.business.SelaiApplication;
import com.selai.business.base.BaseActivity;
import com.selai.business.history.adapter.SaleHistoryAdapter;

import android.support.v7.widget.RecyclerView;

import javax.inject.Inject;

import butterknife.BindView;

public class SaleHistoryActivity extends BaseActivity implements SaleHistoryContract.View{

    @BindView(R.id.rv_history)
    RecyclerView rvHistory;

    SaleHistoryAdapter saleHistoryAdapter;

    @Inject
    SaleHistoryPresenter presenter;

    @Override
    public int getLayout() {
        return R.layout.activity_sale_history;
    }

    @Override
    public void setup() {
        bindViewToPresenter();
    }

    public void bindViewToPresenter(){
        presenter.setView(this);
    }

    @Override
    public void initInjection() {
        ((SelaiApplication)getApplication()).getApplicationComponent().Inject(this);
    }

    @Override
    public void showError(String message) {

    }
}

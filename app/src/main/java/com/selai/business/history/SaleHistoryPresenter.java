package com.selai.business.history;

import com.selai.business.base.BasePresenter;

import javax.inject.Inject;

public class SaleHistoryPresenter extends BasePresenter<SaleHistoryContract.View> implements SaleHistoryContract.Presenter {

    @Inject
    public SaleHistoryPresenter(){

    }

    @Override
    public void clearAllSubscription() {

    }
}

package com.selai.business.history.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.CreateSaleModel;

import android.support.annotation.Nullable;

import java.util.List;

public class SaleHistoryAdapter extends BaseQuickAdapter<CreateSaleModel,BaseViewHolder> {

    public SaleHistoryAdapter(int layoutResId,
        @Nullable List<CreateSaleModel> data) {
        super(R.layout.item_store, data);
    }

    public SaleHistoryAdapter(@Nullable List<CreateSaleModel> data) {
        super(data);
    }

    public SaleHistoryAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, CreateSaleModel item) {

    }
}

package com.selai.business.checkout.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.Rekening;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;

public class RekeningAdapter extends ArrayAdapter<Rekening> {

    List<Rekening> data;
    Context context;

    public RekeningAdapter(@NonNull Context context,
        @NonNull List<Rekening> objects) {
        super(context, R.layout.item_rekening, objects);
        this.data = objects;
        this.context = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
        @NonNull ViewGroup parent) {
        TextView tvRekening = (TextView) super.getView(position,convertView,parent);

        Rekening item = data.get(position);
        switch (item.getType()){
            case REGULAR:
                tvRekening.setText(item.getBank()+" a \\ n "+item.getNameRek()+" - "+item.getNoRek());
                tvRekening.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                tvRekening.setTextColor(ContextCompat.getColor(context,R.color.grey_dark_light));
                break;
            case ADD:
                tvRekening.setText(context.getString(R.string.add_new_rekening));
                tvRekening.setTextColor(ContextCompat.getColor(context,R.color.blue_text_lang));
                tvRekening.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add,0,0,0);
                break;
        }

        return tvRekening;

    }

    //    public RekeningAdapter(@Nullable List<Rekening> data) {
//        super(R.layout.item_rekening, data);
//    }
//
//    public RekeningAdapter(int layoutResId) {
//        super(layoutResId);
//    }
//
//    @Override
//    protected void convert(BaseViewHolder helper, Rekening item) {
//        switch (item.getType()){
//            case REGULAR:
//                ivAdd.setVisibility(View.GONE);
//                tvRekening.setText(item.getBank()+" a \\ n "+item.getNameRek()+" - "+item.getNoRek());
//                break;
//            case ADD:
//                ivAdd.setVisibility(View.VISIBLE);
//                tvRekening.setText(mContext.getString(R.string.add_new_rekening));
//                tvRekening.setTextColor(ContextCompat.getColor(mContext,R.color.blue_text_lang));
//                break;
//        }
//    }
}

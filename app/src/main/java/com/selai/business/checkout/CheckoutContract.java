package com.selai.business.checkout;

public interface CheckoutContract {

    interface View{
        void attachView(long money);
    }

    interface Presenter{
        void loadData();
    }

}

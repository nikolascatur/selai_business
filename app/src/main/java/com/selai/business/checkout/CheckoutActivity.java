package com.selai.business.checkout;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.checkout.adapter.RekeningAdapter;
import com.selai.business.model.Rekening;
import com.selai.business.util.Utils;
import com.selai.business.view.CustomSpinner;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class CheckoutActivity extends BaseActivity implements CheckoutContract.View {

    @Inject
    CheckoutPresenter presenter;

    @BindView(R.id.toolbar)
    View toolbar;

    @BindView(R.id.saldo_fill)
    TextView tvSaldoFill;

    @BindView(R.id.et_saldo_checkout)
    EditText etSaldoCheckout;

    @BindView(R.id.cs_rekening)
    CustomSpinner csRekening;

    @BindView(R.id.tv_rekening)
    TextView tvRekening;

    Long saldo = 0L;

    Long saldoCheckout = 0L;

    RekeningAdapter adapter;

    List<Rekening> rekeningList;


    @Override
    public int getLayout() {
        return R.layout.activity_checkout;
    }

    @Override
    public void setup() {
        configToolbar(toolbar, getString(R.string.checkout_money));
        bindViewToPresenter();
        initObserver();
        presenter.loadData();
    }

    public void initObserver() {
//        RxTextView.textChanges(etSaldoCheckout).map(CharSequence::toString).map(s -> s.trim())
//            .subscribe(input -> {
//                Long dtLong = Long.parseLong(input);
//                etSaldoCheckout.setText(Utils.getCurrencyFormat(dtLong));
//            });
    }

    @OnClick(R.id.tv_rekening)
    public void clickRekening() {
        csRekening.performClick();
    }


    @OnClick(R.id.tv_half)
    public void clickHalf() {
        saldoCheckout = saldo / 2;
        etSaldoCheckout.setText(Utils.getCurrencyFormat(saldoCheckout));
    }

    @OnClick(R.id.tv_half_more)
    public void clickHalfMore() {
        saldoCheckout = (saldo * 3) / 4;
        etSaldoCheckout.setText(Utils.getCurrencyFormat(saldoCheckout));
    }

    @OnClick(R.id.tv_all)
    public void clickAll() {
        saldoCheckout = saldo;
        etSaldoCheckout.setText("" + saldoCheckout);
    }

    public void bindViewToPresenter() {
        presenter.setView(this);
    }

    @Override
    public void initInjection() {
        getApplicationComponent().Inject(this);
    }

    public void initLoadRek() {
        rekeningList = new ArrayList<>();
        rekeningList.add(
            new Rekening("Mandiri", "Susan", "08989384023840923", Rekening.TYPE_REKENING.REGULAR));
        rekeningList.add(new Rekening("", "", "", Rekening.TYPE_REKENING.ADD));
        adapter = new RekeningAdapter(this, rekeningList);
        csRekening.setAdapter(adapter);
        Rekening item = rekeningList.get(0);
        tvRekening.setText(item.getBank()+" a \\ n "+item.getNameRek()+" - "+item.getNoRek());
        csRekening.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Rekening rek = rekeningList.get(position);
                switch (rek.getType()){
                    case REGULAR:
                        tvRekening.setText(item.getBank()+" a \\ n "+item.getNameRek()+" - "+item.getNoRek());
                        break;
                    case ADD:

                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void attachView(long money) {
        saldo = money;
        String currency = Utils.getCurrencyFormat(money);
        tvSaldoFill.setText(currency);
        initLoadRek();
    }
}

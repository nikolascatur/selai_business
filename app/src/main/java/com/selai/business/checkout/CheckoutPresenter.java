package com.selai.business.checkout;

import com.selai.business.base.BasePresenter;

import javax.inject.Inject;

public class CheckoutPresenter extends BasePresenter<CheckoutContract.View> implements CheckoutContract.Presenter{

    @Inject
    public CheckoutPresenter(){

    }

    @Override
    public void clearAllSubscription() {

    }

    @Override
    public void loadData() {
        getView().attachView(100000000);
    }
}

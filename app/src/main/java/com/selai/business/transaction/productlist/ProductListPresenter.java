package com.selai.business.transaction.productlist;

import com.selai.business.api.TransactionApi;
import com.selai.business.base.BasePresenter;
import com.selai.business.data.transaction.TransactionExecute;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.response.ProductListResponse;
import com.selai.business.util.Utils;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class ProductListPresenter extends BasePresenter<ProductListContract.View> implements ProductListContract.Presenter {


    TransactionExecute transactionExecute;

    @Inject
    public ProductListPresenter(TransactionExecute transactionExecute ){
        this.transactionExecute = transactionExecute;

    }

    @Override
    public void clearAllSubscription() {

    }

    @Override
    public void executeList(int id, int perPage, int page) {
        transactionExecute.fetchProductListExecute(
            new DisposableObserver<Response<ProductListResponse>>() {
                @Override
                public void onNext(Response<ProductListResponse> baseResponseResponse) {
                    if(baseResponseResponse.code() == 200 || baseResponseResponse.code() == 201){
                        getView().attachView(baseResponseResponse.body().getData());
                    }
                    else{
                        String msgError = Utils.msgError(baseResponseResponse.errorBody());
                        getView().showError(msgError);
                    }

                }

                @Override
                public void onError(Throwable e) {
                    getView().showError(e.getMessage());
                }

                @Override
                public void onComplete() {

                }
            },id,perPage,page,"-created_at");
    }
}

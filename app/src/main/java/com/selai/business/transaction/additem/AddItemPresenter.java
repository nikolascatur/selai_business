package com.selai.business.transaction.additem;

import com.selai.business.base.BasePresenter;
import com.selai.business.data.transaction.TransactionEntity;
import com.selai.business.data.transaction.TransactionExecute;
import com.selai.business.model.Product;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.response.CreateProductResponse;
import com.selai.business.util.Utils;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class AddItemPresenter extends BasePresenter<AddItemContract.View> implements AddItemContract.Presenter {

    TransactionExecute transactionExecute;


    @Inject
    public AddItemPresenter(TransactionExecute transactionExecute){
        this.transactionExecute = transactionExecute;
    }

    @Override
    public void clearAllSubscription() {
        transactionExecute.clearAllSubscription();
    }

    @Override
    public void executeCreateProductSale(int id, Product product) {
        transactionExecute.createProductSaleExecute(
            new DisposableObserver<Response<CreateProductResponse>>() {
                @Override
                public void onNext(Response<CreateProductResponse> baseResponseResponse) {
                    if(baseResponseResponse.code() == 201)
                         getView().onNextScreen();
                    else{
                        String msgError = Utils.msgError(baseResponseResponse.errorBody());
                        getView().showError(msgError);
                    }

                }

                @Override
                public void onError(Throwable e) {
                    getView().showError(e.getMessage());
                }

                @Override
                public void onComplete() {

                }
            },id,product);
    }
}

package com.selai.business.transaction.productlist.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.Size;
import com.selai.business.model.Variant;

import android.support.annotation.Nullable;

import java.util.List;

public class SizeListAdapter extends BaseQuickAdapter<Variant,BaseViewHolder> {

    public SizeListAdapter(
        @Nullable List<Variant> data) {
        super(R.layout.item_product_child, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Variant item) {
        helper.setText(R.id.tv_no,""+helper.getLayoutPosition()+1);
        helper.setText(R.id.tv_size,""+item.getVariant());
        helper.setText(R.id.tv_count,""+item.getStock());
        helper.setText(R.id.tv_weigth,""+item.getWeight());

    }
}

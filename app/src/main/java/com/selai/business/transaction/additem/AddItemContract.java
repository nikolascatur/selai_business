package com.selai.business.transaction.additem;

import com.selai.business.base.BaseContractView;
import com.selai.business.model.Product;

public interface AddItemContract {

    interface View extends BaseContractView{
        void onNextScreen();
    }

    interface Presenter{

        void executeCreateProductSale(int id,Product product);

    }

}

package com.selai.business.transaction.productlist.adapter;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.Product;
import com.selai.business.model.ProductItem;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import java.util.List;

public class ProductListAdapter extends BaseQuickAdapter<ProductItem, BaseViewHolder> {

    SizeListAdapter sizeListAdapter;
    RecyclerView rvAddVariant;

    public ProductListAdapter(@Nullable List<ProductItem> data) {
        super(R.layout.item_product_list, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProductItem item) {
        sizeListAdapter = new SizeListAdapter(item.getVariants());
        rvAddVariant = helper.getView(R.id.rv_add_variant);
        rvAddVariant.setLayoutManager(new LinearLayoutManager(mContext));
        rvAddVariant.setAdapter(sizeListAdapter);
        ImageView ivPhoto = helper.getView(R.id.iv_photo);
        Glide.with(mContext).load(item.getImage()).into(ivPhoto);
        helper.setText(R.id.tv_title, item.getName()).setText(R.id.et_name_product, ""+item.getPrice());
//            .setText(R.id.et_price_sale, ""+item.getDiscount());
    }
}

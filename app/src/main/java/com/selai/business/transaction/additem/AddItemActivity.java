package com.selai.business.transaction.additem;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.model.FlashSaleProduct;
import com.selai.business.model.Product;
import com.selai.business.model.Size;
import com.selai.business.model.TimeModel;
import com.selai.business.model.Variant;
import com.selai.business.transaction.additem.adapter.AddItemAdapter;
import com.selai.business.transaction.create.CreateFlashSaleActivity;
import com.selai.business.transaction.productlist.ProductListActivity;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.ImagePicker;
import com.selai.business.util.ImageTools;
import com.selai.business.util.Utils;
import com.selai.business.view.TakepicProfile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

public class AddItemActivity extends BaseActivity implements AddItemContract.View {


    @BindView(R.id.rv_add_variant)
    RecyclerView rvAddVariant;

    @BindView(R.id.btn_add)
    Button btnAdd;

    @BindView(R.id.iv_switcher)
    ImageView ivSwitcher;

    AddItemAdapter addItemAdapter;

    @Inject
    AddItemPresenter addItemPresenter;


    @BindView(R.id.ll_add_variant)
    LinearLayout llAddVariant;

    List<Size> produucts;

    @BindView(R.id.iv_photo)
    TakepicProfile ivPhoto;

    @BindView(R.id.et_name_product)
    EditText etNameProduct;

    @BindView(R.id.et_price_sale)
    EditText etPriceSale;

    @BindView(R.id.et_discount)
    EditText etDiscount;

    @BindView(R.id.et_weight)
    EditText etWeight;

    public static final int MAX_IMAGE_SIZE = 360;

    public static final int MAX_FILE_SIZE = 500;

    boolean isSwitcherOn = false;

    public static final int PICK_IMAGE = 720;

    public static final String SELECTED_IMAGE_NAME = "selected-image";

    Bitmap imagePhoto;

    String imagePhotoString;

    int paramId = 0;

    @Override
    public int getLayout() {
        return R.layout.activity_additem;
    }

    @Override
    public void setup() {
        bindViewToPresenter();
        getExtra();
        initAdapter();
        updateSwitcher(false);
        initObserver();
    }

    public void getExtra(){
        paramId = getIntent().getIntExtra(GlobalParameter.BundleParam.PARSING_ID_SALE,0);
        Log.d("nikoo","param id "+paramId);
    }

    public void initObserver() {
        Observable<Boolean> discountInputCheker = RxTextView
            .textChanges(etDiscount)
            .map(CharSequence::toString)
            .map(s ->
                {
                    isSwitcherOn = (s.length() > 0);
                    return isSwitcherOn;
                }
            ).distinctUntilChanged();

        addUiSubScription(discountInputCheker.subscribe(this::updateSwitcher));
    }

    public void updateSwitcher(boolean switchOn) {
        ivSwitcher.setImageResource(
            isSwitcherOn ? R.drawable.ic_switcher_on : R.drawable.ic_switcher_off);
    }

    public void initAdapter() {
        setTitle("Tambah Item");
        produucts = new ArrayList<>();
        int weight = etWeight.getText() != null? Integer.parseInt(etWeight.getText().toString()):10;
        Size variant = new Size("M", 1, weight);
        produucts.add(variant);
        addItemAdapter = new AddItemAdapter(produucts);
        addItemAdapter.enableClick(R.id.iv_remove);
        addItemAdapter.setOnItemChildClickListener((adapter, view, position) -> {
            Log.d("nikoo", " position ::  " + position);
            if (view.getId() == R.id.iv_remove) {
                Log.d("nikoo", "action remove :: " + position);
                produucts.remove(position);
                addItemAdapter.notifyDataSetChanged();
            }
        });
        rvAddVariant.setLayoutManager(new LinearLayoutManager(this));
        rvAddVariant.setAdapter(addItemAdapter);

    }

    public void getImageResult(int resultCode, Intent data, Context contextTmp) {
        Boolean isImageResult = ImagePicker
            .getImageFromResult(contextTmp, resultCode, data, SELECTED_IMAGE_NAME);
        if (isImageResult) {
            compressPhotoResult(ImagePicker.getImagePath(contextTmp, SELECTED_IMAGE_NAME),
                contextTmp);
        }
    }

    public void compressPhotoResult(String documentIdPhotoUri, Context contextTmp) {
        Bitmap bitmap = ImageTools
            .getFixedFileSizeBitmap(documentIdPhotoUri, MAX_IMAGE_SIZE, MAX_FILE_SIZE);
        imagePhoto = bitmap;
        setDocumentIdPhotoPerson(bitmap, contextTmp);

    }

    private void setDocumentIdPhotoPerson(Bitmap compressedBitmap, Context contextTmp) {
        String photoImage = ImageTools.bitmapToBase64(compressedBitmap);
        imagePhotoString = photoImage;
        ivPhoto.setImage(compressedBitmap);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != this.RESULT_OK) return;
        switch (requestCode) {
            case PICK_IMAGE:
                getImageResult(resultCode, data, this);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.ll_add_variant)
    public void addVariant() {
        int weight = etWeight.getText() != null? Integer.parseInt(etWeight.getText().toString()):10;
        Size variant = new Size("M", 1, weight);
        produucts.add(variant);
        addItemAdapter.notifyDataSetChanged();
    }

    public void bindViewToPresenter() {
        addItemPresenter.setView(this);
    }

    @OnClick(R.id.iv_photo)
    public void clickImage() {
        pickImageFromGallery();
    }

    public void pickImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        this.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    @OnClick(R.id.btn_add)
    public void btnAdd() {
        showProgress("");
        int weight = etWeight.getText() != null? Integer.parseInt(etWeight.getText().toString()):10;
        for(int i=0;i<produucts.size();i++){
            produucts.get(i).setWeight(weight);
        }
        Product product = new Product(imagePhotoString, etNameProduct.getText().toString(),
            Float.parseFloat(etPriceSale.getText().toString()), Float.parseFloat(etDiscount.getText().toString()), produucts);

        addItemPresenter.executeCreateProductSale(paramId,product);
    }

    @Override
    public void initInjection() {
        getApplicationComponent().Inject(this);
    }

    @Override
    public void onNextScreen() {
        Intent intent = new Intent(this, ProductListActivity.class);
        setResult(GlobalParameter.CallbackCode.RESULT_OK,intent);
        finish();
    }

    @OnClick(R.id.btn_back)
    public void clickBtnBack(){
        onBackPressed();
    }

    @Override
    public void showError(String message) {
        dismissProgress();
        Utils.ToastMessage(this,message,true);
    }
}

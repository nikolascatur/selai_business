package com.selai.business.transaction.create;

import com.selai.business.base.BaseContractView;
import com.selai.business.model.CourierModel;
import com.selai.business.model.CreateFlashsaleModel;

import java.util.List;

public interface CreateFlashSaleContract   {

    public interface View extends BaseContractView{
        void initCourier(List<CourierModel> courierList);
        void nextScreenProcess(int id);
    }

    public interface Presenter{
        void fetchCourierEntity();

        void createFlassale(CreateFlashsaleModel createFlashsaleModel);
    }

}
package com.selai.business.transaction.create;

import com.selai.business.R;
import com.selai.business.SelaiApplication;
import com.selai.business.base.BaseActivity;
import com.selai.business.model.CourierModel;
import com.selai.business.model.CreateFlashsaleModel;
import com.selai.business.model.CreateSaleModel;
import com.selai.business.model.TimeModel;
import com.selai.business.transaction.additem.AddItemActivity;
import com.selai.business.transaction.create.adapter.CourierAdapter;
import com.selai.business.transaction.productlist.ProductListActivity;
import com.selai.business.transaction.schedule.ScheduleActivity;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.Utils;
import com.selai.business.view.CustomSpinner;
import com.selai.business.view.SelectionInputTextView;

import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateFlashSaleActivity extends BaseActivity implements CreateFlashSaleContract.View {

    @Inject
    CreateFlashSalePresenter createFlashSalePresenter;

    public String[] durationArray() {
        duration.put( "1 jam","1");
        duration.put( "2 Jam","2");
        duration.put( "4 jam","4");
        duration.put( "6 jam","6");
        duration.put( "8 jam","8");
        duration.put( "12 jam","12");
        duration.put( "24 jam","24");
        return convertHasmapToArrayString(duration);
    }

    public String[] maxBuyArray() {
        maxBuy.put("Tidak dibatasi","0");
        maxBuy.put("1 pcs / orang","1");
        maxBuy.put("2 pcs / orang","2");
        maxBuy.put("3 pcs / orang","3");
        maxBuy.put("4 pcs / orang","4");
        maxBuy.put("5 pcs / orang","5");
        maxBuy.put("6 pcs / orang","6");
        maxBuy.put("7 pcs / orang","7");
        maxBuy.put("8 pcs / orang","8");
        maxBuy.put("9 pcs / orang","9");
        maxBuy.put("10 pcs /orang","10");
        return convertHasmapToArrayString(maxBuy);
    }

    CourierAdapter courierAdapter;

    @BindView(R.id.rv_courier)
    RecyclerView rvCourier;

    @BindView(R.id.sit_input_date)
    SelectionInputTextView sitInputDate;

    @BindView(R.id.cs_duration)
    CustomSpinner csDuration;

    @BindView(R.id.tv_duration)
    TextView tvDuration;

    @BindView(R.id.cs_buy_whr)
    CustomSpinner csBuyWhere;

    @BindView(R.id.tv_buy_whr)
    TextView tvBuyWhr;

    @BindView(R.id.cs_max_buy)
    CustomSpinner csMaxBuy;

    @BindView(R.id.tv_max_buy)
    TextView tvMaxBuy;

    @BindView(R.id.input_about_sale)
    EditText inputAboutSale;

    @BindView(R.id.in_user_social)
    EditText inUserSocial;

    @BindView(R.id.btn_back)
    ImageView btnBack;


    public final static int REQUEST_CREATE_PRODUCT = 101;

    CreateSaleModel createSaleModel;

    List<CourierModel> courierModelList;


    @Override
    public int getLayout() {
        return R.layout.activity_create_flashsale;
    }

    @Override
    public void setup() {
        getContext();
        initViewToBinding();
        initSpinner();
        createFlashSalePresenter.fetchCourierEntity();
        initView();
    }

    public void getContext(){
        createSaleModel = getIntent().getParcelableExtra(GlobalParameter.BundleParam.BUNDLE_CREATE_SALE);
    }

    public void initView(){
        setTitle("Buat Flash Sale");
        if(createSaleModel != null) {
            sitInputDate.setText(createSaleModel.getStarted_at());
            tvDuration.setText(getDuration(duration,""+createSaleModel.getDurations()));
            inputAboutSale.setText(createSaleModel.getDescription());
            tvBuyWhr.setText(getBuyWhrTxt(createSaleModel.getCatalog_url()));
            inUserSocial.setText(createSaleModel.getCatalog_username());
            tvMaxBuy.setText(getDuration(maxBuy,""+createSaleModel.getMaximum_buy()));
        }
    }

    public String getBuyWhrTxt(String wb){
        if(wb.toLowerCase().indexOf(buyWhere[0].toLowerCase())> -1){
            return buyWhere[0];
        }
        return buyWhere[1];
    }

    public String getDuration(HashMap<String,String> map,String input){
        for (Map.Entry<String, String> durationSingle : map.entrySet()) {
            if(durationSingle.getValue().equals(input))
                return durationSingle.getKey();
        }
        return "";
    }

    @OnClick(R.id.btn_back)
    public void clickBtnBack(){
        onBackPressed();
    }

    HashMap<String, String> duration = new HashMap<>();

    HashMap<String, String> maxBuy = new HashMap<>();

    public List<Integer> getSelection(List<CourierModel> list){
        List<Integer> returValue = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            if(list.get(i).isIs_active()){
                returValue.add(list.get(i).getId());
                Log.d("nikoo"," selection Courier  "+list.get(i).getId());
            }
        }

        return returValue;
    }

    @OnClick(R.id.btn_next)
    public void clickNext(){
        showProgress("");
        int durationParam = Integer.parseInt(duration.get(tvDuration.getText().toString()));
        int maxBuyParam = Integer.parseInt(maxBuy.get(tvMaxBuy.getText().toString()));
        String catalogUrl = tvBuyWhr.getText().toString().equals("Instagram")?"https://instagram.com":inUserSocial.getText().toString();
        String catalogUserName = tvBuyWhr.getText().toString().equals("Instagram")?inUserSocial.getText().toString():"";
        List<Integer> couriers = getSelection(courierAdapter.getData());

        Log.d("nikoo",
            "started at :: "+sitInputDate.getText()+" "+
            " duration :: "+durationParam+" "+
            " description :: "+inputAboutSale.getText().toString()+" "+
            " catalogurl :: "+catalogUrl+" "+
            " username  :: "+ catalogUserName+" "+
            " max buy :: "+maxBuyParam+" "+
            " courier  :: "+couriers.size()+" "+
            true);
        if(createSaleModel == null) {
            createFlashSalePresenter.createFlassale(new CreateFlashsaleModel(
                sitInputDate.getText(),
                durationParam,
                inputAboutSale.getText().toString(),
                catalogUrl,
                catalogUserName,
                maxBuyParam,
                couriers,
                true
            ));
        }
        else{
            Log.d("nikoo"," LLLLLL  "+createSaleModel.getId());
            Intent intent = new Intent(this,ProductListActivity.class);
            intent.putExtra(GlobalParameter.BundleParam.PARSING_ID_SALE,createSaleModel.getId());
            startActivity(intent);
            finish();
        }
    }

    public String[] convertHasmapToArrayString(HashMap<String, String> map) {
        String[] returnValue = new String[map.size()];
        int i = 0;
        for (Map.Entry<String, String> durationSingle : map.entrySet()) {
            returnValue[i++] = durationSingle.getKey();
        }
        return returnValue;

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("nikoo", " requestCodee :: " + requestCode + " resultcode  ::  " + resultCode + "  ");
        if (requestCode == GlobalParameter.CallbackCode.REQUEST_DATE && resultCode ==
            GlobalParameter.CallbackCode.RESULT_OK) {
            TimeModel timeModel = data.getParcelableExtra(GlobalParameter.BundleParam.PARSING_DATE);
            sitInputDate.setText(timeModel.getDate() + " " + timeModel.getTime());
        }
    }

    String[] buyWhere = {"Instagram", "website"};

    public void initSpinner() {
        String[] duration = durationArray();
//            {"1 jam", "2 Jam", "4 jam", "6 jam", "8 jam", "12 jam", "24 jam"};
        ArrayAdapter<String> durationAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,
            duration);
        tvDuration.setText(duration[0]);
        csDuration.setAdapter(durationAdapter);

        csDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String result = csDuration.getSelectedItem().toString();
                tvDuration.setText(result);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<String> buyWhereAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item,
            buyWhere);
        tvBuyWhr.setText(buyWhere[0]);
        csBuyWhere.setAdapter(buyWhereAdapter);
        csBuyWhere.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String result = csBuyWhere.getSelectedItem().toString();
                tvBuyWhr.setText(result);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String[] maxBuy = maxBuyArray();
            ArrayAdapter < String > maxBuyAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item,
                maxBuy);
        tvMaxBuy.setText(maxBuy[0]);
        csMaxBuy.setAdapter(maxBuyAdapter);
        csMaxBuy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String result = csMaxBuy.getSelectedItem().toString();
                tvMaxBuy.setText(result);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @OnClick(R.id.tv_duration)
    public void onDuration() {
        csDuration.performClick();
    }

    @OnClick(R.id.tv_buy_whr)
    public void onBuyWhr() {
        csBuyWhere.performClick();
    }

    @OnClick(R.id.tv_max_buy)
    public void onMaxBuy() {
        csMaxBuy.performClick();
    }

    public void initViewToBinding() {
        createFlashSalePresenter.setView(this);
    }

    @Override
    public void initInjection() {
        ((SelaiApplication) getApplication()).getApplicationComponent().Inject(this);

    }

    @OnClick(R.id.sit_input_date)
    public void clickInputDate() {
        Intent intent = new Intent(this, ScheduleActivity.class);
        startActivityForResult(intent, GlobalParameter.CallbackCode.REQUEST_DATE);
    }

    @Override
    public void initCourier(List<CourierModel> courierList) {
        courierModelList = new ArrayList<>();
        courierModelList.addAll(courierList);
        rvCourier.setLayoutManager(new GridLayoutManager(this, 2));
        courierAdapter = new CourierAdapter(courierList);
        rvCourier.setAdapter(courierAdapter);
        if(createSaleModel != null){
            for(CourierModel lstTmp:courierModelList){
                lstTmp.setIs_active(false);
            }
            List<CourierModel> dataExisting = createSaleModel.getCouriers();
            for(CourierModel item :dataExisting){
                for(CourierModel itemFromTop : courierModelList){
                    if(item.getId() == itemFromTop.getId() && item.isIs_active()){
                        itemFromTop.setIs_active(true);
                    }
                }
            }
            courierAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void nextScreenProcess(int id) {
        Log.d("nikoo"," show next screen processs  ");
        dismissProgress();
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra(GlobalParameter.BundleParam.PARSING_ID_SALE,id);
        startActivity(intent);
        finish();
    }

    @Override
    public void showError(String message) {
        dismissProgress();
        Utils.ToastMessage(this,message,false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dismissProgress();
    }
}

package com.selai.business.transaction.productlist;

import com.selai.business.base.BaseContractView;
import com.selai.business.model.ProductItem;

import java.util.List;

public interface ProductListContract {

    interface View extends BaseContractView{
        void attachView(List<ProductItem> productList);
    }

    interface Presenter{

        void executeList(int id,int perPage,int page);

    }

}

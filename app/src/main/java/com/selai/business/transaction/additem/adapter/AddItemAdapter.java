package com.selai.business.transaction.additem.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.FlashSaleProduct;
import com.selai.business.model.Size;
import com.selai.business.model.Variant;

import android.support.annotation.Nullable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddItemAdapter extends BaseQuickAdapter<Size,BaseViewHolder> {

    private List<Integer> childListener = new ArrayList<>();


    public AddItemAdapter(@Nullable List<Size> data) {
        super(R.layout.item_variant, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Size item) {
        helper.setText(R.id.et_size,""+item.getVariant()).setText(R.id.et_stock,""+item.getStock());
        for(Integer i:childListener){
            helper.addOnClickListener(i);
            Log.d("nikoo"," integer :: "+i);
        }

    }

    public void enableClick(Integer... ids){
        childListener.clear();
        childListener.addAll(Arrays.asList(ids));

    }
}

package com.selai.business.transaction.productlist;

import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.model.Product;
import com.selai.business.model.ProductItem;
import com.selai.business.model.TimeModel;
import com.selai.business.transaction.additem.AddItemActivity;
import com.selai.business.transaction.productlist.adapter.ProductListAdapter;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.Utils;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ProductListActivity extends BaseActivity implements ProductListContract.View {


    public final static int ADDITEM_REQUEST = 1;

    @BindView(R.id.rv_item_product)
    RecyclerView rvItemProduct;

    @BindView(R.id.btn_add)
    Button btnAdd;

    @BindView(R.id.btn_next)
    Button btnNext;

    ProductListAdapter productListAdapter;
    List<ProductItem> productList;

    @Inject
    ProductListPresenter presenter;

    int paramId = 0;

    @Override
    public int getLayout() {
        return R.layout.activity_product_list;
    }

    @Override
    public void setup() {
        bindViewToPresenter();
        getExtra();
        initAdapter();
        setTitle("List Produk");
        btnNext.setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismissProgress();
    }

    public void bindViewToPresenter(){
        presenter.setView(this);
    }

    public void getExtra(){
        paramId = getIntent().getIntExtra(GlobalParameter.BundleParam.PARSING_ID_SALE,-1);
        if(paramId > -1){
            Log.d("nikoo"," DDDDD  :: "+paramId);
            presenter.executeList(paramId,1,20);
        }
    }

    public void initAdapter(){
        productList = new ArrayList<>();
        productListAdapter = new ProductListAdapter(productList);
        rvItemProduct.setLayoutManager(new LinearLayoutManager(this));
        rvItemProduct.setAdapter(productListAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("nikoo", " requestCodee :: " + requestCode + " resultcode  ::  " + resultCode + "  ");
        if (resultCode == GlobalParameter.CallbackCode.RESULT_OK) {
            presenter.executeList(paramId,1,20);
        }
    }

    @OnClick(R.id.btn_back)
    public void clickBtnBack(){
        onBackPressed();
    }

    @OnClick(R.id.btn_next)
    public void clickNext(){

        finish();
    }

    @Override
    public void initInjection() {
        getApplicationComponent().Inject(this);
    }

    @OnClick(R.id.btn_add)
    public void addBtn(){
        Intent intent = new Intent(this, AddItemActivity.class);
        intent.putExtra(GlobalParameter.BundleParam.PARSING_ID_SALE,paramId);
        startActivityForResult(intent,ADDITEM_REQUEST);
    }


    @Override
    public void attachView(List<ProductItem> productList) {
        this.productList.addAll(productList);
        productListAdapter.notifyDataSetChanged();
        btnNext.setEnabled(this.productList.size()>0);
    }

    @Override
    public void showError(String message) {
        dismissProgress();
        Utils.ToastMessage(this,message,true);
    }
}

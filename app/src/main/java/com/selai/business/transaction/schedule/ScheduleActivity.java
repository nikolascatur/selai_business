package com.selai.business.transaction.schedule;

import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.model.TimeModel;
import com.selai.business.transaction.create.CreateFlashSaleActivity;
import com.selai.business.util.GlobalParameter;
import com.selai.business.view.horizontalcalendar.HorizontalCalendar;
import com.selai.business.view.horizontalcalendar.HorizontalCalendarView;
import com.selai.business.view.horizontalcalendar.model.CalendarEvent;
import com.selai.business.view.horizontalcalendar.utils.CalendarEventsPredicate;
import com.selai.business.view.horizontalcalendar.utils.HorizontalCalendarListener;
import com.selai.business.view.horizontalcalendar.utils.RecyclerViewClickListener;
import com.selai.business.view.horizontalcalendar.utils.Utils;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.OnClick;

public class ScheduleActivity extends BaseActivity implements ScheduleContract.View {

    HorizontalCalendar horizontalCalendar;

    @Inject
    SchedulePresenter schedulePresenter;

    public String dateNow = "";
    public String timeBook = "";

    @Override
    public int getLayout() {
        return R.layout.activity_schedule;
    }

    @Override
    public void setup() {
        initViewToPresenter();
        initCaleder();
        setTitle("Booking Jadwal Sale \t \t \t WIB");
    }

    public void initViewToPresenter(){
        schedulePresenter.setView(this);
    }


    public void initCaleder(){
        schedulePresenter.executeFetchSchedule();
//        Calendar startDate = Calendar.getInstance();
    }

    @OnClick(R.id.btn_back)
    public void clickBtnBack(){
        onBackPressed();
    }

    @OnClick(R.id.btn_pesan)
    public void clickPesan(){
        Intent intent = new Intent(this, CreateFlashSaleActivity.class);
        intent.putExtra(GlobalParameter.BundleParam.PARSING_DATE,new TimeModel(dateNow,timeBook+":00"));
        setResult(GlobalParameter.CallbackCode.RESULT_OK,intent);
        finish();
    }

    @OnClick(R.id.btn_cancel)
    public void clickCancel(){

    }



    @Override
    public void initInjection() {
        getApplicationComponent().Inject(this);
    }

    @Override
    public void attachViewSchedule(HashMap<String, HashMap<String, Boolean>> mapResult) {

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.DATE,1);
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        HorizontalCalendar.Builder horizontalCalendarBuilder   = new HorizontalCalendar.Builder(this, R.id.calendarView)
            .range(startDate, endDate)
            .datesNumberOnScreen(3)
            .configure()
            .showTopText(false)
            .formatMiddleText("EEEE")
            .colorTextMiddle(ContextCompat.getColor(this,R.color.grey_dark_light), ContextCompat.getColor(this,R.color.grey_dark_light))
            .colorTextBottom(ContextCompat.getColor(this,R.color.grey_dark_light), ContextCompat.getColor(this,R.color.grey_dark_light))
            .formatBottomText("dd-MM-yyyy")
            .end()
            .addEvents(date -> {
                String dtNow = date.get(Calendar.YEAR)+"-"+Utils.getTwoDigit(""+(date.get(Calendar.MONTH)+1))+"-"+Utils.getTwoDigit(""+date.get(Calendar.DAY_OF_MONTH));
                List<CalendarEvent> clock = new ArrayList<>();
                for(int i=0;i<24;i++){
                    String time = Utils.getLabelClock(""+i);
                    boolean isCanBook = false;
                    if(mapResult.get(dtNow) != null)
                        isCanBook = mapResult.get(dtNow).get(time);

                    CalendarEvent.TYPE type = isCanBook?CalendarEvent.TYPE.CANTBOOK:CalendarEvent.TYPE.FREE;
                    if(dtNow.equals(dateNow) && clock.equals(time))
                        type = CalendarEvent.TYPE.BOOK;
                    clock.add(new CalendarEvent(dtNow,R.color.grey_dark_light,time,type));
                }
                return clock;
            });

        horizontalCalendar = horizontalCalendarBuilder.build(new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position, String dateNow, String clockNow) {
                ScheduleActivity.this.dateNow = dateNow;
                ScheduleActivity.this.timeBook = clockNow;
                Log.d("nikoo"," date :: "+dateNow+" clockNow :: "+clockNow);
            }
        });

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                Log.d("nikoo"," calendar selected "+date.get(Calendar.DAY_OF_MONTH)+"-"+date.get(Calendar.MONTH));
            }

            public void onCalendarScroll(HorizontalCalendarView calendarView,
                int dx, int dy) {

            }
        });
    }
}

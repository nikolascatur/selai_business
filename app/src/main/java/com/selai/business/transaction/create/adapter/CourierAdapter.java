package com.selai.business.transaction.create.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.CourierModel;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import java.util.List;

public class CourierAdapter extends BaseQuickAdapter<CourierModel,BaseViewHolder>{



    public CourierAdapter(@Nullable List<CourierModel> data) {
        super(R.layout.item_courier,data);
    }

    public CourierAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, CourierModel item) {
        helper.setText(R.id.tv_name_courier,item.getName());

        ImageView ivMark = helper.getView(R.id.iv_mark);
        updateMark(item.isIs_active(),ivMark);
        ivMark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setIs_active(!item.isIs_active());
                updateMark(item.isIs_active(),ivMark);
            }
        });
    }

    public void updateMark(boolean isActive,ImageView ivMark){
        ivMark.setImageResource(isActive?R.drawable.oval_check:R.drawable.oval_grey_outline);
    }
}

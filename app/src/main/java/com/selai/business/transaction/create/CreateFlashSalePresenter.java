package com.selai.business.transaction.create;

import com.selai.business.base.BasePresenter;
import com.selai.business.data.account.CourierEntity;
import com.selai.business.data.repository.CourierRepository;
import com.selai.business.data.transaction.TransactionExecute;
import com.selai.business.model.CreateFlashsaleModel;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.response.CourierInquiryResponse;
import com.selai.business.network.response.CreateSaleResponse;
import com.selai.business.util.Utils;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class CreateFlashSalePresenter extends BasePresenter<CreateFlashSaleContract.View>
    implements CreateFlashSaleContract.Presenter {


    CourierEntity courierEntity;

    CourierRepository courierRepository;

    TransactionExecute transactionExecute;

    @Inject
    public CreateFlashSalePresenter(CourierEntity courierEntity,
        CourierRepository courierRepository,TransactionExecute transactionExecute) {
        this.courierEntity = courierEntity;
        this.courierRepository = courierRepository;
        this.transactionExecute = transactionExecute;
    }

    @Override
    public void clearAllSubscription() {

    }

    @Override
    public void fetchCourierEntity() {
        if (courierRepository.getCourierModelList().size() > 0) {
            getView().initCourier(courierRepository.getCourierModelList());
        } else {
            courierEntity.executePost(new DisposableObserver<Response<CourierInquiryResponse>>() {
                @Override
                public void onNext(Response<CourierInquiryResponse> courierInquiryResponseResponse) {
                    Log.d("nikoo", "on next "/*+courierInquiryResponseResponse.body().getData()*/);
                    if(courierInquiryResponseResponse.code() == 200 || courierInquiryResponseResponse.code() == 201)
                         getView().initCourier(courierInquiryResponseResponse.body().getData());
                    else{
                        String msgError = Utils.msgError(courierInquiryResponseResponse.errorBody());
                        getView().showError(msgError);
                    }
                }

                @Override
                public void onError(Throwable e) {
                    Log.d("nikoo", "error :: " + e.getMessage());
                }

                @Override
                public void onComplete() {
                    Log.d("nikoo", "Complete ");
                }
            }, 10, 1, "", "", "");
        }
    }

    @Override
    public void createFlassale(CreateFlashsaleModel createFlashsaleModel) {
        transactionExecute.createFlashsaleExecute(new DisposableObserver<Response<CreateSaleResponse>>() {
            @Override
            public void onNext(Response<CreateSaleResponse> baseResponseResponse) {
//                Log.d("nikoo"," resul next "+baseResponseResponse.code()+"  message  "+baseResponseResponse.message());
//                Log.d("nikoo"," response message :: "+baseResponseResponse.body().getMessages());
//                Log.d("nikoo"," response body"+baseResponseResponse.body().isSuccess());
//                if(baseResponseResponse.code())
                Log.d("nikoo"," error code :: "+baseResponseResponse.code());
                if(baseResponseResponse.code() == 200 || baseResponseResponse.code() == 201){
                    getView().nextScreenProcess(baseResponseResponse.body().getData().getId());
                }
                else{
                    String msgError = Utils.msgError(baseResponseResponse.errorBody());
                    Log.d("nikoo","error msg :: "+msgError);
                    getView().showError(msgError);
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d("nikoo"," message error :: "+e.getMessage());
                getView().showError(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        },createFlashsaleModel);
    }
}

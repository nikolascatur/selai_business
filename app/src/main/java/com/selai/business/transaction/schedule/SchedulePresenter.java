package com.selai.business.transaction.schedule;

import com.selai.business.base.BasePresenter;
import com.selai.business.data.account.CourierEntity;
import com.selai.business.data.transaction.TransactionEntity;
import com.selai.business.network.response.ScheduleResponse;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class SchedulePresenter extends BasePresenter<ScheduleContract.View> implements ScheduleContract.Presenter {

    CourierEntity courierEntity;

    @Inject
   public SchedulePresenter(CourierEntity courierEntity){
       this.courierEntity = courierEntity;
   }

    @Override
    public void clearAllSubscription() {

    }

    @Override
    public void executeFetchSchedule() {
        courierEntity.executeFetchSchedule(new DisposableObserver<Response<ScheduleResponse>>() {
            @Override
            public void onNext(Response<ScheduleResponse> scheduleResponseResponse) {
                if(scheduleResponseResponse.code() == 200){
                    getView().attachViewSchedule(scheduleResponseResponse.body().getData());
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d("nikoo"," message error "+e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }
}

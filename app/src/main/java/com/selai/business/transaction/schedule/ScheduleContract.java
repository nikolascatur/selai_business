package com.selai.business.transaction.schedule;

import java.util.HashMap;

public interface ScheduleContract {

    interface View{
        public void attachViewSchedule(HashMap<String,HashMap<String,Boolean>> mapResult);
    }

    interface Presenter{
        public void executeFetchSchedule();

    }

}

package com.selai.business.api;

import com.selai.business.network.response.CityResponse;
import com.selai.business.network.response.ProvinceResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface NonAuthApi {

    @GET("/api/v1/province")
    public Observable<Response<ProvinceResponse>> fetchProvince();

    @GET("/api/v1/city/{province_id}")
    public Observable<Response<CityResponse>> fetchCity(@Path("province_id") String province_id);

}

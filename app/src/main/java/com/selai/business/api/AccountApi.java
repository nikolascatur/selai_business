package com.selai.business.api;

import com.selai.business.network.body.LoginInquiryBody;
import com.selai.business.network.body.PhoneInquiryBody;
import com.selai.business.network.response.CourierInquiryResponse;
import com.selai.business.network.response.LoginInquiryResponse;
import com.selai.business.network.response.PhoneInquiryResponse;
import com.selai.business.network.response.ProfileInquiryResponse;


import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface AccountApi {

    @POST("/api/v1/merchant/phone/inquiry")
    Observable<Response<PhoneInquiryResponse>> phoneInquiry(
        @Body PhoneInquiryBody inquiryBody);

    @POST("/api/v1/merchant/login")
    Observable<Response<LoginInquiryResponse>> loginInQuiry(
        @Body LoginInquiryBody loginInquiryBody);

    @PUT("/api/v1/merchant/profile")
    Observable<Response<ProfileInquiryResponse>> profileInquiry();

    @GET("/api/v1/merchant/profile")
    Observable<Response<LoginInquiryResponse>> fetchProfile();


}

package com.selai.business.api;

import com.selai.business.model.CreateFlashsaleModel;
import com.selai.business.model.Product;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.response.CourierInquiryResponse;
import com.selai.business.network.response.CreateProductResponse;
import com.selai.business.network.response.CreateSaleResponse;
import com.selai.business.network.response.FlashSaleResponse;
import com.selai.business.network.response.ProductListResponse;
import com.selai.business.network.response.ScheduleResponse;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TransactionApi {

    @GET("/api/v1/merchant/courier")
    Observable<Response<CourierInquiryResponse>> courierInquiry(
        @Query("per_page") int perPage,
        @Query("page") int Page,
        @Query("order_by") String orderBy,
        @Query("name") String name,
        @Query("code") String code
    );

    @POST("/api/v1/merchant/refresh/token")
    Call<ResponseBody> refreshAut();

    @POST("/api/v1/merchant/flash/sale/create")
    Observable<Response<CreateSaleResponse>> createFlashSaleInquiry(
        @Body CreateFlashsaleModel createFlashsaleModel);

    @GET("/api/v1/merchant/flash/sale/schedule")
    Observable<Response<ScheduleResponse>> fetchSchedule();

    @GET("/api/v1/merchant/flash/sale/list")
    Observable<Response<FlashSaleResponse>> fetchListFlashSale(@Query("per_page") int per_page,
        @Query("page") int page, @Query("order_by") String order_by);

    @POST("/api/v1/merchant/flash/sale/product/{id}")
    Observable<Response<CreateProductResponse>> createProduct(@Path("id") int id, @Body Product product);

    @GET("/api/v1/merchant/flash/sale/product/{id}")
    Observable<Response<ProductListResponse>> fetchListProduct(@Path("id") int id,
        @Query("per_page") int perPage, @Query("page") int page,
        @Query("order_by") String order_by);

    @GET("/api/v1/merchant/flash/sale/product/{idsale}/{idproduct}")
    Observable<Response<BaseResponse>> fetchProduct(@Path("idsale") String idsale,
        @Path("idproduct") String idproduct);

    @DELETE("/api/v1/merchant/flash/sale/delete/{id}")
    Observable<Response<BaseResponse>> deleteMerchantSale(@Path("id") int deleteId);

}

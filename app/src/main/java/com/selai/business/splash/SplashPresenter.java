package com.selai.business.splash;

import com.selai.business.base.BasePresenter;
import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.data.execute.NonAuthExecute;
import com.selai.business.data.repository.LocationRepository;
import com.selai.business.network.response.ProvinceResponse;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class SplashPresenter extends BasePresenter<SplashContract.View> implements SplashContract
    .Presenter {

    NonAuthExecute nonAuthExecute;

    LocationRepository locationRepository;

    AccountRepository accountRepository;

    @Inject
    public SplashPresenter(NonAuthExecute nonAuthExecute, LocationRepository locationRepository,AccountRepository accountRepository) {
        this.nonAuthExecute = nonAuthExecute;
        this.locationRepository = locationRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public void clearAllSubscription() {

    }

    @Override
    public void executeGetProvince() {
        nonAuthExecute.executeProvince(new DisposableObserver<Response<ProvinceResponse>>() {
            @Override
            public void onNext(Response<ProvinceResponse> provinceResponseResponse) {
                if (provinceResponseResponse.code() == 200) {
                    locationRepository.initProvinceList(provinceResponseResponse.body().getData());
                    locationRepository.setSuccessLoadProvince(true);
                } else {
                    locationRepository.setSuccessLoadProvince(false);
                }
                getView().processNextGetProvince();
            }

            @Override
            public void onError(Throwable e) {
                locationRepository.setSuccessLoadProvince(false);
                getView().processNextGetProvince();
            }

            @Override
            public void onComplete() {
                getView().processNextGetProvince();
            }
        });
    }

    @Override
    public boolean isAlreadyLogin() {
        return accountRepository.hasToken();
    }

    @Override
    public String getToken() {
        return accountRepository.getToken();
    }


}

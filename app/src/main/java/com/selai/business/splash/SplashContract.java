package com.selai.business.splash;

public interface SplashContract {

    interface View{
        public void processNextGetProvince();
    }

    interface Presenter{

        public void executeGetProvince();

        public boolean isAlreadyLogin();

        String getToken();

    }

}

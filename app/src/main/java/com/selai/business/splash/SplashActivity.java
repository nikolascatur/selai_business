package com.selai.business.splash;

import com.selai.business.R;
import com.selai.business.account.login.LoginActivity;
import com.selai.business.account.password.InputPasswordActivity;
import com.selai.business.base.BaseActivity;
import com.selai.business.dashboard.DashboardActivity;
import com.selai.business.util.GlobalParameter;

import android.content.Intent;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class SplashActivity extends BaseActivity implements SplashContract.View{

    final static int TIMER = 3;

    @Inject
    SplashPresenter splashPresenter;

    @Override
    public int getLayout() {
        return R.layout.splash;
    }

    @Override
    public void setup() {
        bindViewToPresenter();
        initObserver();
    }

    public void initObserver() {
        Observable.timer(TIMER, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(l->splashPresenter.executeGetProvince());
    }

    public void bindViewToPresenter(){
        splashPresenter.setView(this);
    }


    public void nextScreen() {
        if(splashPresenter.isAlreadyLogin()){
            Intent intent = new Intent(this, DashboardActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(GlobalParameter.BundleParam.TOKEN_PARSING, splashPresenter.getToken());
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void initInjection() {
        getApplicationComponent().Inject(this);
    }

    @Override
    public void processNextGetProvince() {
        nextScreen();
    }
}

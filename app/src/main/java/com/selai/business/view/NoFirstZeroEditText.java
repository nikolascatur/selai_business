package com.selai.business.view;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by ykhdzr on 12/28/16.
 */

public class NoFirstZeroEditText extends EditText {

    public NoFirstZeroEditText(Context context) {
        super(context);
        init();
    }

    public NoFirstZeroEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NoFirstZeroEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);

        // Never allow 0 to occur at first character
        if (text.length() > 0) {
            StringBuilder sbResult = new StringBuilder(text);
            if (start == 0 && text.charAt(start) == '0') {
                sbResult.deleteCharAt(start);
                setText(sbResult.toString());
                setSelection(start);
            }
        }
    }
}

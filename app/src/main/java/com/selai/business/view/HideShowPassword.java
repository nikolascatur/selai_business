package com.selai.business.view;

import com.selai.business.R;
import com.selai.business.view.rich.BaseRichView;

import android.content.Context;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.OnClick;

public class HideShowPassword extends BaseRichView {

    @BindView(R.id.iv_eye)
    ImageView ivEye;

    boolean isShowPassword = false;

    EditText editText1;

    EditText editText2;

    public HideShowPassword(Context context) {
        super(context);
    }

    public HideShowPassword(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HideShowPassword(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HideShowPassword(Context context, AttributeSet attrs, int defStyleAttr,
        int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setEditTextLinking(EditText editText1, EditText editText2) {
        this.editText1 = editText1;
        this.editText2 = editText2;
    }

    @OnClick(R.id.iv_eye)
    public void showHidePassword() {
        isShowPassword = !isShowPassword;
        ivEye.setImageResource(isShowPassword ? R.drawable.ic_eye_show : R.drawable.ic_eye_hide);
        updateViewPassword();
    }

    public void updateViewPassword() {
        if (editText1 != null)
            editText1.setTransformationMethod(isShowPassword ? HideReturnsTransformationMethod
                .getInstance() : PasswordTransformationMethod.getInstance());
        if (editText2 != null)
            editText2.setTransformationMethod(isShowPassword ? HideReturnsTransformationMethod
                .getInstance() : PasswordTransformationMethod.getInstance());
    }

    @Override
    public int getLayout() {
        return R.layout.component_eye;
    }

    @Override
    public void setup() {

    }
}

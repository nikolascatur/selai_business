package com.selai.business.view;

import com.bumptech.glide.Glide;
import com.selai.business.R;
import com.selai.business.base.BaseFrameLayout;
import com.selai.business.model.MerchantModel;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

public class ItemStoreComponent extends BaseFrameLayout {

    @BindView(R.id.iv_merchant_profile)
    ImageView ivMerchantProfile;

    @BindView(R.id.tv_desc_product)
    TextView tvDescProduct;

    @BindView(R.id.tv_detail_product)
    TextView tvDetailProduct;

    @BindView(R.id.iv_like)
    ImageView ivLike;

    MerchantModel merchantModel;


    public ItemStoreComponent(@NonNull Context context,MerchantModel merchantModel) {
        super(context);
        this.merchantModel = merchantModel;
        initView();
        Log.d("nikoo"," constructor merchantModel :: "+merchantModel);
    }

    public ItemStoreComponent(@NonNull Context context,
        @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ItemStoreComponent(@NonNull Context context, @Nullable AttributeSet attrs,
        int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ItemStoreComponent(@NonNull Context context, @Nullable AttributeSet attrs,
        int defStyleAttr,
        int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setValue(MerchantModel merchantModel){
        this.merchantModel = merchantModel;
        Log.d("nikoo", "setValuee :: "+merchantModel);
    }

    @Override
    public int getLayout() {
        return R.layout.item_store;
    }

    @Override
    public void setup() {
//        initView();
    }

    public void initView(){
        Log.d("nikoo","init view image "+merchantModel);
        Glide.with(getContext()).load(merchantModel.getImage()).into(ivMerchantProfile);
        tvDescProduct.setText(merchantModel.getShop_name());
        tvDetailProduct.setText(merchantModel.getCity_name()+","+merchantModel.getCountry());

    }

}

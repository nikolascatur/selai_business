package com.selai.business.view.horizontalcalendar.adapter;

import com.selai.business.R;
import com.selai.business.view.horizontalcalendar.HorizontalCalendar;
import com.selai.business.view.horizontalcalendar.model.CalendarEvent;
import com.selai.business.view.horizontalcalendar.utils.RecyclerViewClickListener;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import static com.selai.business.view.horizontalcalendar.model.CalendarEvent.TYPE.BOOK;
import static com.selai.business.view.horizontalcalendar.model.CalendarEvent.TYPE.CANTBOOK;
import static com.selai.business.view.horizontalcalendar.model.CalendarEvent.TYPE.FREE;


/**
 * @author Mulham-Raee
 * @since v1.3.2
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {

    private List<CalendarEvent> eventList;

    private RecyclerViewClickListener listener;

    public EventsAdapter(List<CalendarEvent> eventList,RecyclerViewClickListener listener) {
        this.eventList = eventList;
        this.listener = listener;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_event_button,parent,false);

        return new EventViewHolder(context,itemView,listener);
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.bindData(eventList.get(position));
    }

    public CalendarEvent getItem(int position) throws IndexOutOfBoundsException {
        return eventList.get(position);
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public void update(List<CalendarEvent> eventList) {
        this.eventList = eventList;
        notifyDataSetChanged();
    }

    static class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvTime;
        Context context;
        CalendarEvent calendarEvent;

        private RecyclerViewClickListener listener;

        EventViewHolder(Context context,View itemView,RecyclerViewClickListener listener) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tv_time);
            this.context = context;
            this.listener = listener;
            itemView.setOnClickListener(this);
        }

        public void bindData(CalendarEvent calEvent){
            tvTime.setText(calEvent.getDescription());
            setTextAppearance(calEvent.getBookType().ordinal());
            this.calendarEvent = calEvent;
//            tvTime.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(calEvent.getBookType() != CANTBOOK) {
////                        calEvent.setBookType(calEvent.getBookType() == FREE ? BOOK : FREE);
////                        setTextAppearance(calEvent.getBookType().ordinal());
//                        HorizontalCalendar.selectDate = calEvent.getDateMy();
//                        HorizontalCalendar.selectClock = calEvent.getDescription();
//                    }
//                }
//            });
        }

        public void setTextAppearance(int type){
            int style = R.style.RoundedButton_tworad_roundedGrey;
            tvTime.setBackgroundResource(R.drawable.grey_rounded_2);
            switch (CalendarEvent.TYPE.values()[type]){
                case BOOK:
                    style = R.style.RoundedButton_tworad_roundedBlue;
                    tvTime.setBackgroundResource(R.drawable.blue_rounded_2);
                    break;
                case CANTBOOK:
                    style = R.style.RoundedButton_tworad_roundedPink;
                    tvTime.setBackgroundResource(R.drawable.red_rounded_2);
                    break;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tvTime.setTextAppearance(style);
            }
            else {
                tvTime.setTextAppearance(context,style);
            }
            tvTime.refreshDrawableState();
        }

        @Override
        public void onClick(View v) {
            if(calendarEvent.getBookType() != CANTBOOK) {
                calendarEvent.setBookType(calendarEvent.getBookType() == FREE ? BOOK : FREE);
                setTextAppearance(calendarEvent.getBookType().ordinal());
            }
            listener.onClick(v,getAdapterPosition(),calendarEvent.getDateMy(),calendarEvent.getDescription());
        }
    }
}



package com.selai.business.view.rich;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import butterknife.ButterKnife;

public abstract class BaseRichView extends FrameLayout {

    protected Context context;
    protected AttributeSet attributeSet;

    protected View view;

    public BaseRichView(Context context) {
        super(context);
        init(context, null);
    }

    public BaseRichView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BaseRichView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BaseRichView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs) {
        view = LayoutInflater.from(context).inflate(getLayout(), this, true);
        ButterKnife.bind(this);
        this.context = context;
        this.attributeSet = attrs;
        setup();
    }

    public abstract int getLayout();

    public abstract void setup();

}
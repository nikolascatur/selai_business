package com.selai.business.view;


import com.selai.business.R;
import com.selai.business.view.rich.BaseRichView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

public class TakepicProfile extends BaseRichView {

    @BindView(R.id.img_photo)
    ImageView imgPhoto;

    @BindView(R.id.tv_desc_cam)
    TextView tvDescCam;

    @BindView(R.id.cam_logo)
    ImageView camLogo;

    public TakepicProfile(Context context) {
        super(context);
    }

    public TakepicProfile(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TakepicProfile(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TakepicProfile(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public int getLayout() {
        return R.layout.component_takepicture;
    }

    @Override
    public void setup() {
        TypedArray setting = context.obtainStyledAttributes(attributeSet,R.styleable.TakepicProfile,0,0);
        tvDescCam.setText(setting.getString(R.styleable.TakepicProfile_text));
    }

    public void setImage(Bitmap image){
//        camLogo.setVisibility(View.GONE);
//        tvDescCam.setVisibility(View.GONE);
        imgPhoto.setImageBitmap(image);
    }
}

package com.selai.business.view.horizontalcalendar.utils;

import android.view.View;

public interface RecyclerViewClickListener {

    void onClick(View view,int position,String dateNow,String clockNow);

}

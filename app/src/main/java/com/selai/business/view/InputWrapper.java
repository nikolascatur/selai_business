package com.selai.business.view;


import com.selai.business.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

public class InputWrapper extends LinearLayout {

    private static final Interpolator FAST_OUT_LINEAR_IN_INTERPOLATOR =
            new FastOutLinearInInterpolator();
    private static final Interpolator LINEAR_OUT_SLOW_IN_INTERPOLATOR =
            new LinearOutSlowInInterpolator();

    private static final int ANIMATION_DURATION = 200;

    private LinearLayout mIndicatorArea;
    private int mIndicatorsAdded;

    private boolean mErrorEnabled;
    TextView mErrorView;
    private int mErrorTextAppearance;
    private boolean mErrorShown;
    private CharSequence mError;

    private View mInput;
    private int mInputBackground;
    private int mInputBackgroundError;

    private boolean mUsePadding;

    public InputWrapper(Context context) {
        super(context);
    }

    public InputWrapper(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public InputWrapper(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        setOrientation(VERTICAL);
        refreshDrawableState();

        TypedArray ta = context.obtainStyledAttributes(
                attrs, R.styleable.InputWrapper, 0, 0);
        try {
            setEnabled(ta.getBoolean(
                    R.styleable.InputWrapper_iwEnabled, true));
            mErrorTextAppearance = ta.getResourceId(
                    R.styleable.InputWrapper_iwErrorTextAppearance, 0);
            mErrorEnabled = ta.getBoolean(
                    R.styleable.InputWrapper_iwErrorEnabled, true);
            mInputBackground = ta.getResourceId(
                    R.styleable.InputWrapper_iwInputBackground, 0);
            mInputBackgroundError = ta.getResourceId(
                    R.styleable.InputWrapper_iwInputBackgroundError, mInputBackground);
            mUsePadding = ta.getBoolean(R.styleable.InputWrapper_iwUsePadding, false);
        } finally {
            ta.recycle();
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if(getChildCount() > 0)
            setInput(getChildAt(getChildCount() - 1));
        setErrorEnabled(mErrorEnabled, true);
    }

    private void setInput(View view) {
        mInput = view;
        setInputBackground(mInputBackground);
    }

    private void setInputBackground(int background) {
        if (mUsePadding) {
            int paddingTop = mInput.getPaddingTop();
            int paddingBottom = mInput.getPaddingBottom();
            int paddingLeft = mInput.getPaddingLeft();
            int paddingRight = mInput.getPaddingRight();
            mInput.setBackgroundResource(background);
            mInput.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        } else {
            mInput.setBackgroundResource(background);
        }
    }

    public void setErrorEnabled(boolean enabled) {
        setErrorEnabled(enabled, false);
    }

    public void setErrorEnabled(boolean enabled, boolean forced) {
        if (forced || mErrorEnabled != enabled) {
            if (mErrorView != null) {
                mErrorView.animate().cancel();
            }

            if (enabled) {
                mErrorView = new AppCompatTextView(getContext());
                boolean useDefaultColor = mErrorTextAppearance == 0;
                if (!useDefaultColor) {
                    try {
                        TextViewCompat.setTextAppearance(mErrorView, mErrorTextAppearance);

                        if (Build.VERSION.SDK_INT >= 23
                                && mErrorView.getTextColors().getDefaultColor() == Color.MAGENTA) {
                            // Caused by our theme not extending from Theme.Design*. On API 23 and
                            // above, unresolved theme attrs result in MAGENTA rather than an exception.
                            // Flag so that we use a decent default
                            useDefaultColor = true;
                        }
                    } catch (Exception e) {
                        // Caused by our theme not extending from Theme.Design*. Flag so that we use
                        // a decent default
                        useDefaultColor = true;
                    }
                }
                if (useDefaultColor) {
                    // Probably caused by our theme not extending from Theme.Design*. Instead
                    // we manually set something appropriate
//                    TextViewCompat.setTextAppearance(mErrorView,
//                            R.style.SmallFont);
//                    mErrorView.setTextColor(ContextCompat.getColor(getContext(),
//                            R.color.error_color));
                }
                mErrorView.setVisibility(GONE);
                ViewCompat.setAccessibilityLiveRegion(mErrorView,
                        ViewCompat.ACCESSIBILITY_LIVE_REGION_POLITE);
                addIndicator(mErrorView, 0);
            } else {
                mErrorShown = false;
                removeIndicator(mErrorView);
                mErrorView = null;
            }
            mErrorEnabled = enabled;
        }
    }

    public void setError(@Nullable final CharSequence error) {
        // Only animate if we're enabled, laid out, and we have a different error message
        setError(error, ViewCompat.isLaidOut(this)
                && (mErrorView == null || !TextUtils.equals(mErrorView.getText(), error)));
    }

    private void setError(@Nullable final CharSequence error, final boolean animate) {
        mError = error;

        if (!mErrorEnabled) {
            if (TextUtils.isEmpty(error)) {
                // If error isn't enabled, and the error is empty, just return
                return;
            }
            // Else, we'll assume that they want to enable the error functionality
            setErrorEnabled(true);
        }

        mErrorShown = !TextUtils.isEmpty(error);

        // Cancel any on-going animation
        mErrorView.animate().cancel();

        if (mErrorShown) {
            mErrorView.setText(error);
            mErrorView.setVisibility(VISIBLE);
            setInputBackground(mInputBackgroundError);

            if (animate) {
                if (mErrorView.getAlpha() == 1f) {
                    // If it's currently 100% show, we'll animate it from 0
                    mErrorView.setAlpha(0f);
                }
                mErrorView.animate()
                        .alpha(1f)
                        .setDuration(ANIMATION_DURATION)
                        .setInterpolator(LINEAR_OUT_SLOW_IN_INTERPOLATOR)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animator) {
                                mErrorView.setAlpha(1f);
                            }
                        }).start();
            } else {
                // Set alpha to 1f, just in case
                mErrorView.setAlpha(1f);
            }
        } else {
            setInputBackground(mInputBackground);
            if (mErrorView.getVisibility() == VISIBLE) {
                if (animate) {
                    mErrorView.animate()
                            .alpha(0f)
                            .setDuration(ANIMATION_DURATION)
                            .setInterpolator(FAST_OUT_LINEAR_IN_INTERPOLATOR)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    mErrorView.setText(error);
                                    mErrorView.setVisibility(GONE);
                                }
                            }).start();
                } else {
                    mErrorView.setText(error);
                    mErrorView.setVisibility(GONE);
                }
            }
        }
    }

    private void addIndicator(TextView indicator, int index) {
        if (mIndicatorArea == null) {
            mIndicatorArea = new LinearLayout(getContext());
            mIndicatorArea.setOrientation(LinearLayout.HORIZONTAL);
            LayoutParams params = new LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT);
            addView(mIndicatorArea, -1, params);

        }
        mIndicatorArea.setVisibility(View.VISIBLE);
        LayoutParams params = new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        params.setMargins(
                0,
                getResources().getDimensionPixelSize(R.dimen.dimen_8dp),
                0,
                0
        );
        mIndicatorArea.addView(indicator, index, params);
        mIndicatorsAdded++;
    }

    public CharSequence getError() {
        return mError;
    }

    private void removeIndicator(TextView indicator) {
        if (mIndicatorArea != null) {
            mIndicatorArea.removeView(indicator);
            if (--mIndicatorsAdded == 0) {
                mIndicatorArea.setVisibility(View.GONE);
            }
        }
    }
}

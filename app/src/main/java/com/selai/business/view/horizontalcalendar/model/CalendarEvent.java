package com.selai.business.view.horizontalcalendar.model;

/**
 * @author Mulham-Raee
 * @since v1.3.2
 */
public class CalendarEvent {


    public String getDateMy() {
        return dateMy;
    }

    public void setDateMy(String dateMy) {
        this.dateMy = dateMy;
    }

    public enum TYPE {
        FREE,
        CANTBOOK,
        BOOK
    }

    private String dateMy;
    private TYPE bookType;
    private int color;
    private String description;

    public CalendarEvent(int color){
        this.color = color;
    }

    public CalendarEvent(String dateMy,int color, String description,TYPE bookType) {
        this.dateMy = dateMy;
        this.color = color;
        this.description = description;
        this.bookType = bookType;
    }

    public TYPE getBookType() {
        return bookType;
    }

    public void setBookType(TYPE bookType) {
        this.bookType = bookType;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.selai.business.view;

import com.selai.business.R;
import com.selai.business.base.BaseFrameLayout;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import butterknife.BindView;

public class SelectionInputTextView extends BaseFrameLayout {

    @BindView(R.id.tv_fake_text)
    TextView tvFakeText;

    public SelectionInputTextView(@NonNull Context context) {
        super(context);
    }

    public SelectionInputTextView(@NonNull Context context,
        @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectionInputTextView(@NonNull Context context, @Nullable AttributeSet attrs,
        int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SelectionInputTextView(@NonNull Context context, @Nullable AttributeSet attrs,
        int defStyleAttr,
        int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

   public void setText(String text){
        tvFakeText.setText(text);
   }

   public String getText(){
        return tvFakeText.getText().toString();
   }

    @Override
    public int getLayout() {
        return R.layout.component_input_next;
    }

    @Override
    public void setup() {
    }
}

package com.selai.business.view;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.selai.business.R;
import com.selai.business.helper.RxObservableHelper;
import com.selai.business.view.rich.BaseRichView;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;


public class PhoneNumberView extends BaseRichView {

    @BindView(R.id.et_phone_number)
    NoFirstZeroEditText etPhoneNumber;

    @BindView(R.id.v_bottom_line)
    View vBottomLine;

    @BindView(R.id.tv_code_country)
    TextView tvCodeCountry;

    @BindView(R.id.tv_invalid_message)
    TextView tvInvalidMessage;

    public PhoneNumberView(Context context) {
        super(context);
        initTextChangesListener();
    }

    public PhoneNumberView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTextChangesListener();
    }

    public PhoneNumberView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTextChangesListener();
    }

    public PhoneNumberView(Context context, AttributeSet attrs, int defStyleAttr,
                           int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initTextChangesListener();
    }

    @Override
    public int getLayout() {
        return R.layout.view_phone_number;
    }

    @Override
    public void setup() {

    }

    @Override
    protected void init(Context context, AttributeSet attrs) {
        super.init(context, attrs);
        initBottomLineState();
    }

    private void initBottomLineState() {
        RxTextView
                .textChanges(etPhoneNumber)
                .map(c -> c.length() > 0)
                .subscribe(this::setAlphaBottomLine);
    }

    private void setAlphaBottomLine(Boolean isSetText) {
        if (isSetText) {
            //tvCodeCountry.setTextColor(Color.WHITE);
            vBottomLine.setAlpha(1.0f);
        } else {
            tvCodeCountry.setTextColor(Color.BLACK);
            vBottomLine.setAlpha(0.2f);
        }
    }

    public void setEnabled(boolean isEnable) {
        etPhoneNumber.setEnabled(isEnable);
        if (isEnable) {
            tvCodeCountry.setTextColor(Color.BLACK);
            etPhoneNumber.setTextColor(Color.BLACK);
            vBottomLine
                    .setBackgroundColor(Color.WHITE);
        } else {
//            tvCodeCountry.setTextColor(ContextCompat.getColor(getContext(), R.color.purply));
//            etPhoneNumber.setTextColor(ContextCompat.getColor(getContext(), R.color.purply));
            tvCodeCountry.setTextColor(Color.BLACK);
            etPhoneNumber.setTextColor(Color.BLACK);
            vBottomLine.setAlpha(0.9f);
//            vBottomLine.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.dotted_line));
        }
    }

    public void onValidState() {
        vBottomLine.setBackgroundColor(Color.BLUE);
        tvInvalidMessage.setVisibility(View.GONE);
    }

    public void onErrorState(String message) {
        tvInvalidMessage.setText(message);
        tvInvalidMessage.setVisibility(View.VISIBLE);
        vBottomLine.setBackgroundColor(Color.GREEN);
    }

    private void initTextChangesListener() {
        RxTextView.textChanges(etPhoneNumber)
                .map(CharSequence::toString)
                .map(s -> s.trim())
                .subscribe(input -> onValidState());
//        RxObservableHelper
//            .textTrim(etPhoneNumber)
//            .subscribe(input -> onValidState());
    }

}

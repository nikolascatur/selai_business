package com.selai.business.base;

import android.content.Context;
import android.widget.BaseExpandableListAdapter;

import java.util.HashMap;
import java.util.List;

/**
 * Created by nikolascatur on 9/20/17.
 */

public abstract class BaseExpandableListPagerAdapter<T> extends BaseExpandableListAdapter {

    private Context context;

    private List<String> listHeader;

    private HashMap<String, List<T>> listData;

    public BaseExpandableListPagerAdapter(Context context, List<String> listHeader,
        HashMap<String, List<T>> listData) {
        this.context = context;
        this.listHeader = listHeader;
        this.listData = listData;
    }

    @Override
    public int getGroupCount() {
        return this.listHeader.size();
    }

    @Override
    public int getChildrenCount(int position) {
        return this.listData.get(this.listHeader.get(position)).size();
    }

    @Override
    public Object getGroup(int positionGroup) {
        return this.listHeader.get(positionGroup);
    }

    @Override
    public Object getChild(int positionGroup, int positionChild) {
        return this.listData.get(this.listHeader.get(positionGroup)).get(positionChild);
    }

    @Override
    public long getGroupId(int positionGroup) {
        return positionGroup;
    }

    @Override
    public long getChildId(int positionGroup, int positionChild) {
        return positionChild;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    protected abstract int getLayoutGroup();

    protected abstract int getLayoutChild();


    public Context getContext() {
        return context;
    }

    public List<String> getListHeader() {
        return listHeader;
    }

    public HashMap<String, List<T>> getListData() {
        return listData;
    }
}

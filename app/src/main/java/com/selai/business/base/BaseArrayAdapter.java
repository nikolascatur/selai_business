package com.selai.business.base;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by nikolascatur on 09/02/18.
 */

public abstract class BaseArrayAdapter<I,T> extends ArrayAdapter<T> {
    protected Context context;
    protected LayoutInflater layoutInflater;
    protected int resource;
    protected List<T> items;


    public BaseArrayAdapter(@NonNull Context context, @LayoutRes int resource,
        @NonNull List<T> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.items = objects;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
        @NonNull ViewGroup parent) {
        return createItemView(position,convertView,parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position,convertView,parent);
    }

    public abstract View createItemView(int position,View convertView,ViewGroup parent);
}

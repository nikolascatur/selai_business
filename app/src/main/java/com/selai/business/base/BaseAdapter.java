package com.selai.business.base;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by nikolascatur on 22/03/18.
 */

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseViewHolder<T>> {

    private Context context;

    private List<T> itemList;

    public BaseAdapter(Context context,List<T> itemList){
        this.setContext(context);
        this.setItemList(itemList);
    }

    @Override
    public abstract BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.BindData(getItemList().get(position));
    }

    @Override
    public int getItemCount() {
        return getItemList().size();
    }

    public abstract int getLayout();

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public List<T> getItemList() {
        return itemList;
    }

    public void setItemList(List<T> itemList) {
        this.itemList = itemList;
    }
}

package com.selai.business.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by nikolascatur on 22/03/18.
 */

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder{

    private Context context;
    private View itemView;

    public BaseViewHolder(Context context,View itemView) {
        super(itemView);
        this.setContext(context);
        this.setItemView(itemView);

        ButterKnife.bind(context,itemView);
    }

    public abstract void BindData(T item);

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public View getItemView() {
        return itemView;
    }

    public void setItemView(View itemView) {
        this.itemView = itemView;
    }
}

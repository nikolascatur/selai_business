package com.selai.business.base;

import com.google.gson.Gson;

import com.f2prateek.rx.preferences.RxSharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by nikolascatur on 23/04/18.
 */

public abstract class BasePreferences {

    private final Gson gson;

    private SharedPreferences sharedPreferences;

    private RxSharedPreferences rxSharedPreferences;

    public BasePreferences(Context context, Gson gson) {
        this.gson = gson;
        initPreferences(context, getPreferencesGroup());
    }

    public abstract String getPreferencesGroup();

    public void initPreferences(Context context, String preferencesGroup) {
        sharedPreferences = context.getSharedPreferences(preferencesGroup, 0);
        rxSharedPreferences = RxSharedPreferences.create(sharedPreferences);
    }

    protected void savedata(String tag, int value) {
        rxSharedPreferences.getInteger(tag).set(value);
    }

    protected void savedata(String tag, float value) {
        rxSharedPreferences.getFloat(tag).set(value);
    }

    protected void savedata(String tag, String value) {
        rxSharedPreferences.getString(tag).set(value);
    }

    protected void savedata(String tag, long value) {
        rxSharedPreferences.getLong(tag).set(value);
    }

    protected void savedata(String tag, Boolean value) {
        rxSharedPreferences.getBoolean(tag).set(value);
    }

    protected <T> void savedata(String tag, T obj) {
        rxSharedPreferences.getString(tag).set(gson.toJson(obj));
    }

    protected String getString(String tag) {
        return rxSharedPreferences.getString(tag).get();
    }

    protected <T> T getObjectData(String tag,Class<T> classOf){
        String rawData = rxSharedPreferences.getString(tag).get();
        return gson.fromJson(rawData,classOf);
    }


}

package com.selai.business.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import butterknife.ButterKnife;

/**
 * Created by nikolascatur on 19/01/18.
 */

public abstract class BaseView extends FrameLayout {

    public BaseView(Context context) {
        super(context);
        init(context,null);
    }

    public BaseView(Context context,AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BaseView(Context context, AttributeSet attrs,int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs);
    }

    public void init(Context context, AttributeSet attr){
        LayoutInflater.from(context).inflate(getLayout(),this,true);
        ButterKnife.bind(this);
    }

    public abstract int getLayout();
}

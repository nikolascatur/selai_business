package com.selai.business.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.selai.business.network.base.RetrofitHelper;

import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nikolascatur on 17/01/18.
 */

public abstract class BaseNetwork<T> {

    private T service = null;

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public HttpLoggingInterceptor interceptor(){
        return new HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    public OkHttpClient.Builder okHttpClientBuilder(OkHttpClient.Builder builder){
        return builder
            .addInterceptor(interceptor());
    }

    public OkHttpClient httpClientBuilder() {
        return okHttpClientBuilder(new OkHttpClient.Builder())
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30,TimeUnit.SECONDS)
            .connectTimeout(30,TimeUnit.SECONDS)
            .build();
    }

//    protected SSLSocketFactory getSSL() {
//        try {
//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            InputStream cert = getAppContext().getResources().openRawResource(R.raw.client);
//            Certificate ca = cf.generateCertificate(cert);
//            cert.close();
//
//            // creating a KeyStore containing our trusted CAs
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//            return new AdditionalKeyStore(keyStore);
//        } catch(Exception e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public Gson gsonProvider(){
        return new GsonBuilder().create();
    }

    public Retrofit provideRetrofit(){
        Gson gson = this.gsonHandler((new GsonBuilder()).setPrettyPrinting()).setDateFormat("yyyy-MM-dd\'T\'hh:mm:ssZ").create();
        return new Retrofit.Builder()
            .baseUrl(getBaseUrl())
            .client(httpClientBuilder())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();
    }

    protected GsonBuilder gsonHandler(GsonBuilder builder) {
        return builder;
    }

    public T networkService(){
        if (service == null) {
            Retrofit retrofit = provideRetrofit();
            RetrofitHelper.init(retrofit);
            service = retrofit.create(getApi());
        }
        return service;
    }

    public void clearAllSubscription(){
        compositeDisposable.clear();
    }

    public Disposable addSubscription(Disposable disposable){
        compositeDisposable.add(disposable);
        return compositeDisposable;
    }


    public abstract String getBaseUrl();

    public abstract Class<T> getApi();

}

package com.selai.business.base;

import com.google.gson.Gson;

import com.f2prateek.rx.preferences.RxSharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;


import rx.Observable;

/**
 * Created by randiwaranugraha on 12/23/16.
 */

public abstract class AbstractPreferences {

    private final Gson gson;

    private SharedPreferences sharedPreferences;

    private RxSharedPreferences rxSharedPreferences;

    public AbstractPreferences(Context context, Gson gson) {
        this.gson = gson;
        initPreferences(context, getPreferencesGroup());
    }

    public abstract String getPreferencesGroup();

    private void initPreferences(Context context, String preferencesGroup) {
        sharedPreferences = context.getSharedPreferences(preferencesGroup, 0);
        rxSharedPreferences = RxSharedPreferences.create(sharedPreferences);
    }

    protected RxSharedPreferences getRxSharedPreferences() {
        return rxSharedPreferences;
    }

    protected void saveData(String tag, String value) {
        rxSharedPreferences.getString(tag).set(value);
    }

    protected void saveData(String tag, Long value) {
        rxSharedPreferences.getLong(tag).set(value);
    }

    protected void saveData(String tag, Boolean value) {
        rxSharedPreferences.getBoolean(tag).set(value);
    }

    protected void saveData(String tag, Integer value) {
        rxSharedPreferences.getInteger(tag).set(value);
    }

    protected void saveData(String tag, Float value) {
        rxSharedPreferences.getFloat(tag).set(value);
    }

    protected <T> void saveData(String tag, T obj) {
        saveData(tag, gson.toJson(obj));
    }

    protected <T> T getData(String tag, Class<T> classOfT) {
        String rawData = rxSharedPreferences.getString(tag).get();
        return gson.fromJson(rawData, classOfT);
    }

    protected <T> Observable<T> getDataAsObservable(String tag, Class<T> classOfT) {
        return rxSharedPreferences.getString(tag)
            .asObservable()
            .map(rawData -> gson.fromJson(rawData, classOfT));
    }

    public Observable<Boolean> clearData(String tag) {
        return Observable.defer(() -> {
            if (sharedPreferences != null) {
                sharedPreferences.edit().remove(tag).apply();
                return Observable.just(true);
            }
            return Observable.just(false);
        });
    }

    protected String getString(String tag) {
        return rxSharedPreferences.getString(tag, null).get();
    }

    protected long getLong(String tag) {
        Long lng = rxSharedPreferences.getLong(tag).get();
        if (lng == null) {
            return 0;
        }
        return lng;
    }

    protected boolean getBoolean(String tag) {
        Boolean value = rxSharedPreferences.getBoolean(tag).get();
        if (value == null) {
            return false;
        }
        return value;
    }



    protected int getInteger(String tag) {
        Integer integer = rxSharedPreferences.getInteger(tag).get();
        if (integer == null) {
            return 0;
        }
        return integer;
    }



    protected float getFloat(String tag) {
        Float fl = rxSharedPreferences.getFloat(tag).get();
        if (fl == null) {
            return 0f;
        }
        return fl;
    }


}
package com.selai.business.base;


import com.jakewharton.rxbinding2.widget.RxAdapterView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.selai.business.R;
import com.selai.business.SelaiApplication;
import com.selai.business.di.component.ApplicationComponent;
import com.selai.business.util.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Created by nikolascatur on 14/01/18.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private CompositeDisposable uiSubscription;

    private ProgressDialog progressDialog;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getApplicationComponent().Inject(this);
        initCompoisteDisposable();
        findViews();
        initInjection();
        setup();
        initFirebase();
    }

    public void initCompoisteDisposable() {
        if (uiSubscription == null || uiSubscription.isDisposed()) {
            uiSubscription = new CompositeDisposable();
        }
    }

    private void initFirebase() {
    }

    public CompositeDisposable addUiSubScription(Disposable disposable) {
        uiSubscription.add(disposable);
        return uiSubscription;
    }

    public void clearAllSubscription() {
        uiSubscription.clear();
    }

    @Override
    protected void onStart() {

        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void baseSubscribe(Object subscribe) {

    }

    public void findViews() {
        if (0 != getLayout()) {
            setContentView(getLayout());
            unbinder = ButterKnife.bind(this);
        }
    }


    public ApplicationComponent getApplicationComponent() {
        return ((SelaiApplication) getApplication()).getApplicationComponent();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        clearAllSubscription();
        super.onDestroy();
    }

    public Observable<Boolean> isEdittextValid(EditText editText, int length) {
        return RxTextView.textChanges(editText)
            .map(text -> text.length() > length)
            .distinctUntilChanged();
    }

    public Observable<Boolean> isSelectedSpinner(Spinner spinner){
        return Observable.just(spinner.getChildCount() > 0);
    }

    public Observable<Boolean> isSpinnerValid(Spinner spinner) {
        return RxAdapterView.itemSelections(spinner).map(
            integer -> {
                Log.d("nikoo"," integer :: "+integer);
                return (spinner.getSelectedItem() != null);
            }

        ).distinctUntilChanged();
    }

    public void showProgress(String title) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            if(title.equals("")) {
                if (null != progressDialog.getWindow()) {
                    progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    progressDialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
                }
                progressDialog.setContentView(R.layout.custom_progressdialog);
            }else {
                progressDialog.setMessage(title);
            }
        }
    }

    public void dismissProgress(){
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void setTitle(String title){
        TextView textView = (TextView) this.findViewById(R.id.tv_toolbar_action);
        if(textView != null){
            textView.setText(title);
        }
    }


    public abstract int getLayout();

    public abstract void setup();

    public abstract void initInjection();


    public void configToolbar(View toolbarTitle,String title) {
        TextView tvToolbar = toolbarTitle.findViewById(R.id.tv_toolbar_action);
        tvToolbar.setText(title);
    }

    public void showError(String message) {
        dismissProgress();
        Utils.ToastMessage(this, message, false);
    }



}

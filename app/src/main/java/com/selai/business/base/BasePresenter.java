package com.selai.business.base;

/**
 * Created by nikolascatur on 15/03/18.
 */

public abstract class BasePresenter<T> {

    private T view;

    public T getView() {
        return view;
    }

    public void setView(T view) {
        this.view = view;
    }

    public abstract void clearAllSubscription();

}

package com.selai.business;

import com.selai.business.di.component.ApplicationComponent;
import com.selai.business.di.component.DaggerApplicationComponent;
import com.selai.business.di.module.ApplicationModule;

import android.support.multidex.MultiDexApplication;

public class SelaiApplication extends MultiDexApplication {
    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initInjection();
    }


    private void initInjection() {
        if (applicationComponent == null) {
            applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        }
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }


}

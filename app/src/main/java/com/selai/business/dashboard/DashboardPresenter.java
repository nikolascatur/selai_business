package com.selai.business.dashboard;

import com.selai.business.base.BasePresenter;
import com.selai.business.data.account.CourierEntity;
import com.selai.business.data.account.ProfileEntity;
import com.selai.business.data.repository.CourierRepository;
import com.selai.business.data.transaction.TransactionEntity;
import com.selai.business.data.transaction.TransactionExecute;
import com.selai.business.model.CreateSaleModel;
import com.selai.business.model.MerchantModel;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.response.CourierInquiryResponse;
import com.selai.business.network.response.FlashSaleResponse;
import com.selai.business.network.response.LoginInquiryResponse;
import com.selai.business.network.response.ProfileInquiryResponse;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class DashboardPresenter extends BasePresenter<DashboardContract.View> implements
    DashboardContract.Presenter {

    CourierEntity courierEntity;

    CourierRepository courierRepository;

    ProfileEntity profileEntity;

    TransactionEntity transactionEntity;

    TransactionExecute transactionExecute;

    int pageFlashSale = 1;

    final static int LIMIT = 10;

    List<CreateSaleModel> listSaleRepository = new ArrayList<>();

    @Inject
    public DashboardPresenter(CourierEntity courierEntity, CourierRepository courierRepository,
        ProfileEntity profileEntity, TransactionEntity transactionEntity,TransactionExecute transactionExecute) {
        this.courierEntity = courierEntity;
        this.courierRepository = courierRepository;
        this.profileEntity = profileEntity;
        this.transactionEntity = transactionEntity;
        this.transactionExecute = transactionExecute;

    }

    @Override
    public void clearAllSubscription() {

    }

    @Override
    public void fetchInitData() {
        profileEntity.addSubscription(Observable.zip(getProfileObservable(),
            getFlashSaleListObservable(LIMIT, pageFlashSale, "-created_at"),
            getCourierObservable(10, 1, "", "", ""),
            (profile, flashsale, courier) -> {
            boolean profileReturn  = false;
            boolean flashSaleReturn = false;
            boolean courrierReturn = false;
            Log.d("nikoo"," profile result "+profile.code());
                if(profile.code() == 200){
                    MerchantModel merchantMap = profile.body().getData().getMerchant();
                    getView().fetchView(merchantMap);
                    profileReturn = true;
                }


                Log.d("nikoo"," flassale result "+flashsale.code());
                if(flashsale.code() == 200){
                    listSaleRepository = flashsale.body().getData();
                    getView().fetchFlashSale(listSaleRepository);
                    flashSaleReturn = true;
                }

                Log.d("nikoo"," courier result "+courier.code());
                if(courier.code() == 200){
                    courierRepository.setCourierModelList(courier.body().getData());
                    courrierReturn = true;
                }
                return profileReturn && flashSaleReturn && courrierReturn;
            }).subscribe(isValid -> {
                if(!isValid){
                    getView().dismisProgress();
                }
        }, throwable -> {
                Log.d("nikoo","error message "+throwable.getMessage());
                getView().showError(throwable.getMessage());

        }));

    }

    public Observable<Response<FlashSaleResponse>> getFlashSaleListObservable(int perPage, int page,
        String orderBy) {
        return transactionEntity.fetchFlasSaleInquiry(perPage, page, orderBy)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread());
    }


    public Observable<Response<LoginInquiryResponse>> getProfileObservable() {
        return profileEntity.getFetchProfile()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response<CourierInquiryResponse>> getCourierObservable(int perpage, int page,
        String orderBY, String name, String code) {
        return courierEntity.courierInquiry(perpage, page, orderBY, name, code)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public void fetchCourier() {
        courierEntity.executePost(new DisposableObserver<Response<CourierInquiryResponse>>() {
            @Override
            public void onNext(Response<CourierInquiryResponse> courierInquiryResponseResponse) {
                if (courierInquiryResponseResponse.code() == 200) {
                    courierRepository.setCourierModelList(
                        courierInquiryResponseResponse.body().getData());
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        }, 10, 1, "", "", "");
    }

    @Override
    public void fetchMerchant() {
        profileEntity.executePost(new DisposableObserver<Response<LoginInquiryResponse>>() {
            @Override
            public void onNext(Response<LoginInquiryResponse> profileInquiryResponseResponse) {
                if (profileInquiryResponseResponse.code() == 200) {
                    MerchantModel merchantMap = profileInquiryResponseResponse.body().getData()
                        .getMerchant();
                    getView().fetchView(merchantMap);
                    fetchCourier();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.d("nikoo", " message error :: " + e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void deleteMerchantSale(int id) {
        transactionExecute.deleteMerchant(new DisposableObserver<Response<BaseResponse>>() {
            @Override
            public void onNext(Response<BaseResponse> baseResponseResponse) {
                if(baseResponseResponse.code() == 200 || baseResponseResponse.code() == 201){
                    Log.d("nikoo","delete success");
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        },id);
    }
}

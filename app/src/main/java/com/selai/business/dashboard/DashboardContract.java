package com.selai.business.dashboard;

import com.selai.business.base.BaseContractView;
import com.selai.business.model.CreateSaleModel;
import com.selai.business.model.MerchantModel;

import java.util.List;

public interface DashboardContract  {

    public interface View extends BaseContractView{
        public void onNextCourier();
        public void fetchView(MerchantModel merchantModel);
        void fetchFlashSale(List<CreateSaleModel> saleList);
        void dismisProgress();
    }

    public interface Presenter{


        void fetchInitData();

        public void fetchCourier();

        public void fetchMerchant();

        void deleteMerchantSale(int id);

    }

}

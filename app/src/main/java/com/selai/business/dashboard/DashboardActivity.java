package com.selai.business.dashboard;

import com.selai.business.R;
import com.selai.business.SelaiApplication;
import com.selai.business.base.BaseActivity;
import com.selai.business.dashboard.adapter.SaleItemAdapter;
import com.selai.business.model.CreateSaleModel;
import com.selai.business.model.MerchantModel;
import com.selai.business.model.OwnerModel;
import com.selai.business.profile.ProfileActivity;
import com.selai.business.setting.SettingActivity;
import com.selai.business.transaction.create.CreateFlashSaleActivity;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.Utils;
import com.selai.business.view.ItemStoreComponent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Interceptor;

public class DashboardActivity extends BaseActivity implements DashboardContract.View {

    @BindView(R.id.ll_top)
    LinearLayout llTop;

    @BindView(R.id.tv_active_sale)
    TextView tvActiveSale;

    @BindView(R.id.rv_list_sale)
    RecyclerView rvListSale;

    @BindView(R.id.ll_empty)
    LinearLayout llEmpty;

    @BindView(R.id.btn_create_flashsale)
    Button btnCreateFlashsale;

    String token = "";

    @BindView(R.id.item_history)
    View itemHistory;

    @BindView(R.id.btn_action)
    ImageView btnAction;

    @Inject
    DashboardPresenter presenter;

//    MerchantModel merchantModel;

    //    OwnerModel ownerModel;
    SaleItemAdapter saleItemAdapter;

    List<CreateSaleModel> saleList;


    @Override
    public int getLayout() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void setup() {
        bindViewToPresenter();
        getExtras();
        initView();
        showProgress("");
        initItemHistory();
//        presenter.fetchMerchant();
        presenter.fetchInitData();
    }


    @OnClick(R.id.btn_action)
    public void clickBtnSetting(){
        startActivity(new Intent(this,SettingActivity.class));
    }


    public void initItemHistory(){
        ImageView ivIcon  = itemHistory.findViewById(R.id.iv_merchant_profile);
        TextView tvTitle = itemHistory.findViewById(R.id.tv_desc_product);
        TextView tvDetailProduct = itemHistory.findViewById(R.id.tv_detail_product);
        ImageView ivNext = itemHistory.findViewById(R.id.iv_like);

        ivIcon.setImageResource(R.drawable.ic_bag_blue);
        tvTitle.setText(getString(R.string.sale_history));
        tvDetailProduct.setText(getString(R.string.view_history));
        ivNext.setOnClickListener(v -> {

        });
    }

    public void bindViewToPresenter() {
        presenter.setView(this);
    }

    @OnClick(R.id.btn_create_flashsale)
    public void clickCreateBtn() {
        Intent intent = new Intent(this, CreateFlashSaleActivity.class);
        startActivity(intent);
    }

    public void getExtras() {
        Bundle bundle = getIntent().getExtras();
        token = bundle.getString(GlobalParameter.BundleParam.TOKEN_PARSING);
//        merchantModel = bundle.getParcelable(GlobalParameter.BundleParam.PARSING_MERCHANT);
//        Log.d("nikoo"," check merchant "+merchantModel);
//        ownerModel = bundle.getParcelable(GlobalParameter.BundleParam.PARSING_OWNER);
    }

    public void initView() {
        rvListSale.setLayoutManager(new LinearLayoutManager(this));
        saleList = new ArrayList<>();
        saleItemAdapter = new SaleItemAdapter(saleList);
        rvListSale.setAdapter(saleItemAdapter);
    }

    @Override
    public void initInjection() {
        ((SelaiApplication) getApplication()).getApplicationComponent().Inject(this);
    }

    @Override
    public void onNextCourier() {

    }

    @Override
    public void fetchView(MerchantModel merchantModel) {
        dismissProgress();
        Log.d("nikoo", " merchant model :: " + merchantModel.getCity_name());
        ItemStoreComponent itemStoreComponent = new ItemStoreComponent(this, merchantModel);
        llTop.addView(itemStoreComponent);
        itemStoreComponent.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void fetchFlashSale(List<CreateSaleModel> saleList) {
        Log.d("nikoo","kok gak masukk ");
        llEmpty.setVisibility(saleList.size()>0? View.GONE:View.VISIBLE);
        this.saleList.addAll(saleList);
        saleItemAdapter.notifyDataSetChanged();
    }

    @Override
    public void dismisProgress() {
        dismissProgress();
    }

}

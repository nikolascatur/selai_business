package com.selai.business.dashboard.adapter;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.CreateSaleModel;
import com.selai.business.transaction.create.CreateFlashSaleActivity;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.Utils;

import android.content.Intent;
import android.media.Image;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SaleItemAdapter extends BaseQuickAdapter<CreateSaleModel,BaseViewHolder> {

    List<Integer> childListener = new ArrayList<>();

    public SaleItemAdapter(
        @Nullable List<CreateSaleModel> data) {
        super(R.layout.item_store, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CreateSaleModel item) {
        ImageView ivImage = helper.getView(R.id.iv_merchant_profile);

//        simpleDateFormat.

        helper.setText(R.id.tv_desc_product,"#"+helper.getLayoutPosition()+" Sale - "+ Utils
            .changeFormatString(item.getStarted_at()));
        ImageView ivLike = helper.getView(R.id.iv_like);

        ivLike.setOnClickListener(v -> {
            Intent intent = new Intent(mContext,CreateFlashSaleActivity.class);
            intent.putExtra(GlobalParameter.BundleParam.BUNDLE_CREATE_SALE,mData.get(helper.getLayoutPosition()));
            mContext.startActivity(intent);
        });

        for(Integer i: childListener){
            helper.addOnClickListener(i);
        }

    }

    public void enableClick(Integer... id){
        childListener.clear();
        childListener.addAll(Arrays.asList(id));
    }
}

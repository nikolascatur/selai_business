package com.selai.business.data.account;

import com.selai.business.BuildConfig;
import com.selai.business.api.AccountApi;
import com.selai.business.data.AbstractNetwork;
import com.selai.business.data.util.RxUtil;
import com.selai.business.network.body.PhoneInquiryBody;
import com.selai.business.network.response.PhoneInquiryResponse;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class PhoneEntity extends AbstractNetwork<AccountApi> {

    @Inject
    public PhoneEntity(){

    }

    @Override
    public Class<AccountApi> getApi() {
        return AccountApi.class;
    }

    private Observable<Response<PhoneInquiryResponse>> phoneInquiry(String phone) {
        return networkService()
            .phoneInquiry(new PhoneInquiryBody(phone))
            .map(phoneInquiryResponse -> phoneInquiryResponse);
    }

    public Disposable executePost(DisposableObserver<Response<PhoneInquiryResponse>> observer, String phone) {
        return addSubscription(phoneInquiry(phone)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }


}

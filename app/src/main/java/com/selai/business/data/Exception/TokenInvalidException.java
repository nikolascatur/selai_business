package com.selai.business.data.Exception;

import android.content.Context;

import java.io.IOException;

/**
 * Created by mexanjuadha on 3/9/17.
 */

public class TokenInvalidException extends IOException  {

    private Context context;

    public TokenInvalidException(Context context) {
        super("Token invalid");
        this.context = context;
    }

}
package com.selai.business.data.repository;

import com.selai.business.model.CourierModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CourierRepository {

    private int page;
    private List<CourierModel> courierModelList = new ArrayList<>();

    @Inject
    public CourierRepository(){

    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<CourierModel> getCourierModelList() {
        return courierModelList;
    }

    public void setCourierModelList(List<CourierModel> courierModelList) {
        this.courierModelList = courierModelList;
    }
}

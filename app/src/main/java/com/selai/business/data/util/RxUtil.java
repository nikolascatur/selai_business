package com.selai.business.data.util;


import com.selai.business.data.Exception.ExceptionParser;
import com.selai.business.data.Exception.GeneralException;
import com.selai.business.data.Exception.NetworkCertificateException;
import com.selai.business.data.Exception.NoNetworkException;
import com.selai.business.data.Exception.TokenInvalidException;

import android.content.Context;

import java.security.cert.CertificateException;

import javax.net.ssl.SSLPeerUnverifiedException;

import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.Observable;


public class RxUtil {

    private static final ObservableTransformer threadTransformer =
        observable -> ((Observable) observable)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .compose(applyCertificateTransformer());

    private static final ObservableTransformer serviceTransformer =
        observable -> ((Observable) observable)
            .compose(applyThreadTransformer())
            .flatMap(new ExceptionParser<>());

    private static Context staticContext;

    private static final ObservableTransformer certificateTransformer =
        observable -> ((Observable) observable)
            .onErrorResumeNext(
                new Function<Throwable, ObservableSource>() {
                    @Override
                    public ObservableSource apply(Throwable throwable) throws Exception {
                        if (throwable.getCause() instanceof CertificateException || throwable instanceof
                            SSLPeerUnverifiedException) {
                            return Observable.error(new NetworkCertificateException(staticContext));
                        } else if (!isSpecificException(throwable)) {
                            return Observable.error(new GeneralException(staticContext));
                        }
                        return Observable.error(throwable);
                    }
                });


    private RxUtil() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    private static boolean isNoNetworkException(Throwable throwable) {
        return throwable instanceof NoNetworkException;
    }

    private static boolean isTokenValidException(Throwable throwable) {
        return throwable instanceof TokenInvalidException;
    }

    private static boolean isSpecificException(Throwable throwable) {
        return isNoNetworkException(throwable) || isTokenValidException(throwable);
    }

    public static void init(Context context) {
        staticContext = context;
    }

    /**
     * Get {@link rx.Observable.Transformer} that transforms the source observable to subscribe in
     * the worker thread and observe on the Android's UI thread.
     *
     * Because it doesn't interact with the emitted items it's safe ignore the unchecked casts.
     *
     * @return {@link rx.Observable.Transformer}
     */
    @SuppressWarnings("unchecked")
    public static <T> ObservableTransformer<T, T> applyThreadTransformer() {
        return (ObservableTransformer<T, T>) threadTransformer;
    }

    /**
     * Apply {@see com.paypro.data.utils.RxUtil#applyThreadTransformer()} and intercept error
     * exception with {@see com.paypro.data.network.exception.ExceptionParser}.
     *
     * Because it doesn't interact with the emitted items it's safe ignore the unchecked casts.
     *
     * @return {@link rx.Observable.Transformer}
     */
    @SuppressWarnings("unchecked")
    public static <T> ObservableTransformer<T, T> applyServiceTransformer() {
        return (ObservableTransformer<T, T>) serviceTransformer;
    }

    /**
     * Get {@link rx.Observable.Transformer} that handle
     * {@link java.security.cert.CertificateException}
     * instead.
     *
     * Because it doesn't interact with the emitted items it's safe ignore the unchecked casts.
     *
     * @return {@link rx.Observable.Transformer}
     */
    @SuppressWarnings("unchecked")
    public static <T> ObservableTransformer<T, T> applyCertificateTransformer() {
        return (ObservableTransformer<T, T>) certificateTransformer;
    }
}
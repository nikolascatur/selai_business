package com.selai.business.data.Exception;

import android.content.Context;

import java.io.IOException;

/**
 * Created by aldo on 2/22/17.
 */

public class NoNetworkException extends IOException {

    private Context context;

    public NoNetworkException(Context context) {
        this.context = context;
    }

}

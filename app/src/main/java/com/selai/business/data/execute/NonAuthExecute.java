package com.selai.business.data.execute;

import com.selai.business.data.Entity.NonAuth;
import com.selai.business.network.response.CityResponse;
import com.selai.business.network.response.ProvinceResponse;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class NonAuthExecute extends NonAuth {

    @Inject
    public NonAuthExecute() {

    }

    public Disposable executeProvince(DisposableObserver<Response<ProvinceResponse>> observer) {
        return addSubscription(fetchProvince()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }

    public Disposable executeCity(DisposableObserver<Response<CityResponse>> observer,String id){
        return addSubscription(fetchCity(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }

}

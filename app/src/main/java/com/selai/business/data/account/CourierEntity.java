package com.selai.business.data.account;

import com.selai.business.api.AccountApi;
import com.selai.business.api.TransactionApi;
import com.selai.business.data.AuthAbstractNetwork;
import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.network.response.CourierInquiryResponse;
import com.selai.business.network.response.ScheduleResponse;

import android.content.Context;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class CourierEntity extends AuthAbstractNetwork<TransactionApi> {

    @Inject
    public CourierEntity(Context context,
        AccountRepository accountRepository) {
        super(context, accountRepository);
    }

    @Override
    public Call<ResponseBody> refreshToken() throws IOException {
        return networkService().refreshAut();
    }

    public Observable<Response<CourierInquiryResponse>> courierInquiry(int perpage, int page,
        String orderBY, String name, String code) {
        return networkService().courierInquiry(perpage, page, orderBY, name, code)
            .map(courierInquiryResponseResponse -> courierInquiryResponseResponse);
    }

    public Disposable executePost(DisposableObserver<Response<CourierInquiryResponse>> disposabel,
        int perpage, int page,
        String orderBY, String name, String code) {
        return addSubscription(courierInquiry(perpage, page, orderBY, name, code)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(disposabel)
        );
    }

    public Observable<Response<ScheduleResponse>> scheduleFetch(){
        return networkService().fetchSchedule().map(scheduleResponseResponse -> scheduleResponseResponse);
    }

    public Disposable executeFetchSchedule(DisposableObserver<Response<ScheduleResponse>> disposabel) {
        return addSubscription(scheduleFetch()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(disposabel)
        );
    }

    @Override
    public Class<TransactionApi> getApi() {
        return TransactionApi.class;
    }
}

package com.selai.business.data.account;

import com.selai.business.BuildConfig;
import com.selai.business.api.AccountApi;
import com.selai.business.base.BaseNetwork;
import com.selai.business.data.AbstractNetwork;
import com.selai.business.data.util.RxUtil;
import com.selai.business.network.body.LoginInquiryBody;
import com.selai.business.network.response.LoginInquiryResponse;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class LoginEntity extends AbstractNetwork<AccountApi> {

    @Inject
    public LoginEntity(){

    }

    @Override
    public Class<AccountApi> getApi() {
        return AccountApi.class;
    }

    private Observable<Response<LoginInquiryResponse>> loginInquiry(String phone, String password) {
        return networkService()
            .loginInQuiry(new LoginInquiryBody(phone, password))
//            .compose(RxUtil.applyServiceTransformer())
            .map(loginInquiryResponse -> loginInquiryResponse);
    }

    public Disposable executePost(DisposableObserver<Response<LoginInquiryResponse>> observer, String phone,
        String password) {
        return addSubscription(loginInquiry(phone, password)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));

    }
}

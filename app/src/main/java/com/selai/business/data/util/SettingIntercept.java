package com.selai.business.data.util;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class SettingIntercept implements Interceptor {

    final static String LANGUAGE = "Accept-Language";
    final static String REGION ="Time-Zone";

    String lang;
    String timeZone;

    public SettingIntercept(String lang,String timeZone){
        this.lang = lang;
        this.timeZone = timeZone;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response request = chain.proceed(addHeader(chain.request()));
        return request;
    }

    public Request addHeader(Request oriRequest){
        oriRequest = oriRequest.newBuilder().addHeader(REGION,timeZone).build();
        return oriRequest.newBuilder().addHeader(LANGUAGE,lang).build();
    }
}

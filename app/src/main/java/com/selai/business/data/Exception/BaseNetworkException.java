package com.selai.business.data.Exception;

import com.selai.business.network.BaseResponse;

import java.util.List;

/**
 * Created by randiwaranugraha on 12/26/16.
 */

public class BaseNetworkException extends Exception {

    private boolean isSuccess;

    private int errorId;

    private List<String> messages;

    private String errorBahasa;

    public BaseNetworkException(BaseResponse baseResponse) {
        setSuccess(baseResponse.isSuccess());
        setErrorId(baseResponse.getStatus());
        setMessages(baseResponse.getMessages());
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public int getErrorId() {
        return errorId;
    }

    public void setErrorId(int errorId) {
        this.errorId = errorId;
    }


    public void setErrorBahasa(String errorBahasa) {
        this.errorBahasa = errorBahasa;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
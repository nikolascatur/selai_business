package com.selai.business.data.interceptor;

import com.selai.business.data.AuthAbstractNetwork;
import com.selai.business.data.Exception.TokenInvalidException;
import com.selai.business.data.account.repository.AccountRepository;

import android.content.Context;
import android.database.Observable;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

public class AuthInterceptor implements Interceptor {

    public static final String AUTHORIZATION = "Authorization";

    private retrofit2.Response responseToken;

    private AuthAbstractNetwork tokenAbstract;

    private static boolean isAlreadyLaunch;

    private static boolean isFetchToken;

    private Context context;

    AccountRepository headerManager;

    public AuthInterceptor(Context context, AccountRepository headerManager,
        AuthAbstractNetwork tokenAbstract) {
        this.context = context;
        this.headerManager = headerManager;
        this.tokenAbstract = tokenAbstract;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request oriRequest = chain.request();
        Log.d("nikoo", " add intercept :: " + headerManager + " valuee " + headerManager.getToken() + "  has token " + headerManager.hasToken());
//        newRefreshToken();
        Log.d("nikoo","after refresh token ");
        if (headerManager.hasToken()) {
            Log.d("nikoo"," response "+chain+" ori rewuest "+oriRequest);
            Response response = chain.proceed(addHeaderAuth(oriRequest));
//            if(response.isSuccessful())

            Log.d("nikoo", "response check " + response.toString()+" isSuccesFull  "+response.isSuccessful());
            return response;
        } else {
//            Response response = chain.proceed(addHeaderAuth(oriRequest));
            try {
                newRefreshToken();
            }
            catch (Exception e){
                e.printStackTrace();
            }
            Response response = chain.proceed(oriRequest);
//            doWhenForbidden(response);
//            Log.d("nikoo","else responsee  :: "+response.toString());
//            responseToken = tokenAbstract.refreshToken().execute();
//            responseToken.headers().
            return response;
        }
    }

    public void newRefreshToken() throws IOException {

        if (!isAlreadyLaunch) {
            isAlreadyLaunch = true;
            responseToken = tokenAbstract.refreshToken().execute();
        }
        Log.d("nikoo"," value response token "+responseToken);
        if (responseToken != null) {
            Log.d("nikoo"," response token "+responseToken.body()+" "+responseToken.message()+" responseToken "+responseToken.headers());
//            String token = responseToken.headers().get()
//            headerManager.saveToken(responseToken.body().toString());
        }

    }

    private void doWhenForbidden(Response response) throws TokenInvalidException {
        int code = response.code();
        if (code == HttpURLConnection.HTTP_FORBIDDEN) {
            throw new TokenInvalidException(context);
        }
    }

    private Request addHeaderAuth(Request oriRequest) {
        Log.d("nikoo",
            "add header :: " + AUTHORIZATION + " header " + headerManager.getBearerToken());
        return oriRequest.newBuilder()
            .addHeader(AUTHORIZATION, headerManager.getBearerToken())
            .build();
    }
}

package com.selai.business.data.account;

import com.selai.business.BuildConfig;
import com.selai.business.api.AccountApi;
import com.selai.business.data.AbstractNetwork;
import com.selai.business.data.AuthAbstractNetwork;
import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.body.PhoneInquiryBody;
import com.selai.business.network.response.LoginInquiryResponse;
import com.selai.business.network.response.PhoneInquiryResponse;
import com.selai.business.network.response.ProfileInquiryResponse;

import android.content.Context;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileEntity extends AuthAbstractNetwork<AccountApi> {


    @Inject
    public ProfileEntity(Context context,
        AccountRepository accountRepository) {
        super(context, accountRepository);
    }

    @Override
    public Class<AccountApi> getApi() {
        return AccountApi.class;
    }

    public Observable<Response<LoginInquiryResponse>> getFetchProfile() {
        return networkService()
            .fetchProfile()
            .map(phoneInquiryResponse -> phoneInquiryResponse);
    }

    public Disposable executePost(DisposableObserver<Response<LoginInquiryResponse>> observer) {
        return addSubscription(getFetchProfile()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }

    @Override
    public Call<ResponseBody> refreshToken() throws IOException {
        return null;
    }
}

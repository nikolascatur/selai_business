package com.selai.business.data.Exception;

import com.selai.business.network.BaseResponse;

import io.reactivex.Observable;
import io.reactivex.functions.Function;


/**
 * Created by randiwaranugraha on 12/26/16.
 */
public class ExceptionParser<T extends BaseResponse> implements Function<T, Observable<T>> {

    @Override
    public Observable<T> apply(T baseResponse) {
        if (baseResponse.isSuccess()) {
            return Observable.just(baseResponse);
        } else {
            return Observable.error(new BaseNetworkException(baseResponse));
        }
    }

//    @Override
//    public Observable<T> call(T baseResponse) {
//        if (baseResponse.isSuccess()) {
//            return Observable.just(baseResponse);
//        } else {
//            return Observable.error(new BaseNetworkException(baseResponse));
//        }
//    }
}
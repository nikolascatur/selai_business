package com.selai.business.data.transaction;

import com.selai.business.api.TransactionApi;
import com.selai.business.data.AuthAbstractNetwork;
import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.model.CreateFlashsaleModel;
import com.selai.business.model.Product;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.response.CreateProductResponse;
import com.selai.business.network.response.CreateSaleResponse;
import com.selai.business.network.response.FlashSaleResponse;
import com.selai.business.network.response.ProductListResponse;

import android.content.Context;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;

public class TransactionEntity extends AuthAbstractNetwork<TransactionApi> {

    @Inject
    public TransactionEntity(Context context,
        AccountRepository accountRepository) {
        super(context, accountRepository);
    }


    public Observable<Response<CreateSaleResponse>> createFlashSaleInquiry(CreateFlashsaleModel createFlashsaleModel){
        return networkService().createFlashSaleInquiry(createFlashsaleModel).map(flashsaleresponse->flashsaleresponse);
    }

    public Observable<Response<FlashSaleResponse>> fetchFlasSaleInquiry(int perPage,int page,String orderBy){
        return networkService().fetchListFlashSale(perPage, page, orderBy).map(flashSaleResponseResponse -> flashSaleResponseResponse);
    }

    public Observable<Response<ProductListResponse>> fetchListProduct(int id,int perPage,int page,String orderBy){
        return networkService().fetchListProduct(id,perPage, page, orderBy).map(flashSaleResponseResponse -> flashSaleResponseResponse);
    }


    public Observable<Response<CreateProductResponse>> createSaleProduct(int id,Product product){
        return networkService().createProduct(id, product).map(flashSaleResponseResponse -> flashSaleResponseResponse);
    }

    public Observable<Response<BaseResponse>> deleteMerchantSale(int id){
        return networkService().deleteMerchantSale(id).map(flashSaleResponseResponse -> flashSaleResponseResponse);
    }


    @Override
    public Call<ResponseBody> refreshToken() throws IOException {
        return null;
    }

    @Override
    public Class<TransactionApi> getApi() {
        return TransactionApi.class;
    }
}

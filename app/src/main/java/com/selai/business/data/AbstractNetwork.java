package com.selai.business.data;


import com.selai.business.BuildConfig;
import com.selai.business.base.BaseNetwork;
import com.selai.business.data.util.SettingIntercept;

import okhttp3.OkHttpClient;

public abstract class AbstractNetwork<T> extends BaseNetwork<T> {

    @Override
    public String getBaseUrl() {
        return BuildConfig.BASE_URL;
    }

    @Override
    public OkHttpClient.Builder okHttpClientBuilder(OkHttpClient.Builder builder) {
        builder.addInterceptor(new SettingIntercept("id","Asia/Jakarta"));
        return super.okHttpClientBuilder(builder);
    }

}

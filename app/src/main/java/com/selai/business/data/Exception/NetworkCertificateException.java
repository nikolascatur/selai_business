package com.selai.business.data.Exception;

import android.content.Context;

import java.security.cert.CertPathValidatorException;

/**
 * Created by mexanjuadha on 3/15/17.
 */

public class NetworkCertificateException extends CertPathValidatorException  {

    private Context context;

    public NetworkCertificateException(Context context) {
        this.context = context;
    }

}
package com.selai.business.data.Entity;

import com.selai.business.api.NonAuthApi;
import com.selai.business.data.AbstractNetwork;
import com.selai.business.network.response.CityResponse;
import com.selai.business.network.response.ProvinceResponse;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;

public class NonAuth extends AbstractNetwork<NonAuthApi> {

    @Inject
    public NonAuth(){

    }


    public Observable<Response<ProvinceResponse>> fetchProvince(){
        return networkService().fetchProvince().map(provinceResponse -> provinceResponse);
    }

    public Observable<Response<CityResponse>> fetchCity(String id){
        return networkService().fetchCity(id).map(cityResponseResponse -> cityResponseResponse);
    }

    @Override
    public Class<NonAuthApi> getApi() {
        return NonAuthApi.class;
    }
}

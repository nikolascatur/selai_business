package com.selai.business.data.transaction;

import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.model.CreateFlashsaleModel;
import com.selai.business.model.Product;
import com.selai.business.network.BaseResponse;
import com.selai.business.network.response.CreateProductResponse;
import com.selai.business.network.response.CreateSaleResponse;
import com.selai.business.network.response.FlashSaleResponse;
import com.selai.business.network.response.ProductListResponse;

import android.content.Context;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class TransactionExecute extends TransactionEntity {

    @Inject
    public TransactionExecute(Context context,
        AccountRepository accountRepository) {
        super(context, accountRepository);
    }

    public Disposable createFlashsaleExecute(DisposableObserver<Response<CreateSaleResponse>> observer,CreateFlashsaleModel createFlashsaleModel){
        return addSubscription(createFlashSaleInquiry(createFlashsaleModel)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }

    public Disposable fetchFlashSaleListExecute(DisposableObserver<Response<FlashSaleResponse>> observer,int perPage,int page,String orderBy){
        return addSubscription(fetchFlasSaleInquiry(perPage,page,orderBy)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }

    public Disposable fetchProductListExecute(DisposableObserver<Response<ProductListResponse>> observer,int id,int perPage,int page,String orderBy){
        return addSubscription(fetchListProduct(id,perPage,page,orderBy)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }


    public Disposable createProductSaleExecute(DisposableObserver<Response<CreateProductResponse>> observer,int id,Product product){
        return addSubscription(createSaleProduct(id, product)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }

    public Disposable deleteMerchant(DisposableObserver<Response<BaseResponse>> observer,int id){
        return addSubscription(deleteMerchantSale(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(observer));
    }
}

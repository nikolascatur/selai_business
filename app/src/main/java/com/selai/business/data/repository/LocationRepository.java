package com.selai.business.data.repository;

import com.google.gson.Gson;

import com.selai.business.base.AbstractPreferences;
import com.selai.business.base.BasePreferences;
import com.selai.business.model.City;
import com.selai.business.model.Province;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class LocationRepository  {


    private List<Province> provinceList = new ArrayList<>();

    private HashMap<String,List<City>> cityList = new HashMap<>();

    private boolean isSuccessLoadProvince = false;

    @Inject
    public LocationRepository(){

    }


    public List<Province> getProvinceList() {
        return provinceList;
    }

    public void initProvinceList(List<Province> provinceList) {
        this.provinceList = provinceList;
    }

    public HashMap<String, List<City>> getCityList() {
        return cityList;
    }

    public void setCityList(HashMap<String, List<City>> cityList) {
        this.cityList = cityList;
    }

    public boolean isSuccessLoadProvince() {
        return isSuccessLoadProvince;
    }

    public void setSuccessLoadProvince(boolean successLoadProvince) {
        isSuccessLoadProvince = successLoadProvince;
    }
}

package com.selai.business.data.Exception;

import android.content.Context;

public class GeneralException extends Exception  {

    private Context context;

    public GeneralException(Context context) {
        this.context = context;
    }

}

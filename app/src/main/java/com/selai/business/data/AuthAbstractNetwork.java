package com.selai.business.data;

import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.data.interceptor.AuthInterceptor;

import android.content.Context;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;

public abstract class AuthAbstractNetwork<T> extends AbstractNetwork<T> {

    private final Context context;
    private final AccountRepository accountRepository;

    public AuthAbstractNetwork(Context context, AccountRepository accountRepository){
        this.context = context;
        this.accountRepository = accountRepository;
    }

    @Override
    public OkHttpClient.Builder okHttpClientBuilder(OkHttpClient.Builder builder){
        builder.addInterceptor(new AuthInterceptor(context, accountRepository,this));
        return super.okHttpClientBuilder(builder);
    }

    public abstract Call<ResponseBody> refreshToken() throws IOException;

    public AccountRepository getHeaderManager() {
        return accountRepository;
    }


}

package com.selai.business.data.account.repository;

import com.google.gson.Gson;

import com.selai.business.BuildConfig;
import com.selai.business.base.AbstractPreferences;
import com.selai.business.network.response.LoginInquiryResponse;
import com.selai.business.util.GlobalParameter;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AccountRepository extends AbstractPreferences {


    private final static String HEADER_TOKEN = "ACCOUNT_MERCHANT";

    private final static String TOKEN_DATA = "ACCOUNT_TOKEN";

    private final static String BEARER = "Bearer";

    @Inject
    public AccountRepository(Context context, Gson gson) {
        super(context, gson);
    }

    @Override
    public String getPreferencesGroup() {
        return GlobalParameter.PreferncesParam.ACCOUNT_PREFERENCES;
    }

    public void saveProfile(LoginInquiryResponse.LoginData userData) {
        saveData(HEADER_TOKEN, userData);
    }

    public void saveToken(String token){
        saveData(TOKEN_DATA,token);
    }

    public String getBearerToken(){
        return BEARER+" "+getToken();
    }

    public String getToken() {
        return getString(TOKEN_DATA);
    }

    public boolean hasToken() {
        if (getToken() !=null && getToken().length() > 0) {
            return true;
        }
        return false;
    }
}

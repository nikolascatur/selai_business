package com.selai.business.helper;

import com.jakewharton.rxbinding2.widget.RxAdapterView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import android.widget.Spinner;
import android.widget.TextView;

import rx.Observable;


public class RxObservableHelper {

//    private static Observable<Integer> textLength(TextView textView) {
//        return textTrim(textView)
//            .map(String::length);
//    }
//
//    public static Observable<Boolean> checkerTextNotNumber(TextView textView) {
//        return textTrim(textView)
//            .map(textInput -> textInput.matches("\\D*"));
//    }
//
//    public static Observable<String> textTrim(TextView textView) {
//        return RxTextView.textChanges(textView)
//            .map(CharSequence::toString)
//            .map(String::trim);
//    }
//
//    public static Observable<Boolean> textInputChangeValidity(TextView textView,
//        String lastChanges) {
//        return textTrim(textView)
//            .distinctUntilChanged()
//            .map(str -> !str.equals(lastChanges));
//    }
//
//    public static Observable<Boolean> textInputLengthValidity(TextView textView) {
//        return textInputLengthValidity(textView, 1);
//    }
//
//    public static Observable<Boolean> textInputLengthValidity(TextView textView, int minLength) {
//        return textLength(textView)
//            .map(lengText -> lengText >= minLength)
//            .distinctUntilChanged();
//    }
//
//    public static Observable<Boolean> spinnerSelectionValidity(Spinner spinner) {
//        return RxAdapterView.itemSelections(spinner)
//            .map(position -> (position > 0));
//    }

//    public static Observable<Boolean> emailValidity(EditText editText) {
//        return RxTextView.textChanges(editText)
//            .map(CharSequence::toString)
//            .map(EmailUtil::isValidEmailAddress)
//            .distinctUntilChanged();
//    }

//    public static Observable<Boolean> dateValidity(EditText editText){
//        return RxTextView.textChanges(editText)
//            .map(CharSequence::toString)
//            .map(s_date -> {
//                if(s_date.matches("\\d{2}/\\d{2}/\\d{4}")){
//                    int dt = Integer.parseInt(s_date.substring(0,2));
//                    int mon = Integer.parseInt(s_date.substring(3,5));
//                    return (dt<=31 && mon <13);
//                }
//
//                return false;
//            })
//            .distinctUntilChanged();
//    }

//    public static Observable<Boolean> emailValidityWithErrMsg(EditText editText, String msg) {
//        return RxTextView.textChanges(editText).map(CharSequence::toString)
//            .map(
//                textInput -> {
//                    if (EmailUtil.isValidEmailAddress(textInput)) {
//                        return true;
//                    }
//                    editText.setError(msg);
//                    return false;
//                }
//            )
//            .distinctUntilChanged();
//    }

//    public static Observable<Boolean> characterMatcher(TextView source, TextView target) {
//        return characterMatcher(source, target.getText().toString().trim());
//    }
//
//    public static Observable<Boolean> characterMatcher(TextView textView, String text) {
//        return textTrim(textView)
//            .map(textInput -> textInput.equals(text));
//    }
}

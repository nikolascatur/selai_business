package com.selai.business.account.login;

import com.selai.business.base.BaseContractView;

import java.util.HashMap;

public interface LoginContract {

    public interface View extends BaseContractView{
        public boolean checkOnNext(HashMap<String, Boolean> condition);

        void closeActivity();

    }

    public interface Presenter{

        void executeLogin(String phoneNumber);

    }

}

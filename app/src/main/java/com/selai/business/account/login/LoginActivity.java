package com.selai.business.account.login;

import com.selai.business.R;
import com.selai.business.SelaiApplication;
import com.selai.business.account.password.CreatePasswordActivity;
import com.selai.business.account.password.InputPasswordActivity;
import com.selai.business.account.register.AddInfoUserActivity;
import com.selai.business.base.BaseActivity;
import com.selai.business.data.account.PhoneEntity;
import com.selai.business.util.CustomClickableSpan;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.TextUtil;
import com.selai.business.network.response.PhoneInquiryResponse;
import com.selai.business.view.NoFirstZeroEditText;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.TextView;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements LoginContract.View {

    @BindView(R.id.et_phone_number)
    NoFirstZeroEditText etPhoneNumber;

    @BindView(R.id.tv_bottom_note)
    TextView tvBottomNote;

    int conditionNext = 0;

    @Inject
    LoginPresenter loginPresenter;


    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void setup() {
        initBindingViewtoPresenter();
        initRegisterView();
    }

    public void initBindingViewtoPresenter(){
        loginPresenter.setView(this);
    }

    public void initRegisterView() {
        String textRegisterHere = getString(R.string.registration_here);
        String textDescRegHere = getString(R.string.desc_here_registration);
        String fullText = textDescRegHere + " " + textRegisterHere;
        SpannableString spannableText = new SpannableString(fullText);
        tvBottomNote.setText(fullText);


        int indexStart = fullText.indexOf(textRegisterHere);
        Log.d("nikoo", " index :: " + indexStart + "  fulltext length :: " + fullText.length());
        Intent intent = new Intent(this, AddInfoUserActivity.class);
        CustomClickableSpan clickableSpan = new CustomClickableSpan(this, intent);
        spannableText.setSpan(clickableSpan, indexStart, indexStart + textRegisterHere.length(), 0);

        tvBottomNote.setText(spannableText);
        tvBottomNote.setMovementMethod(LinkMovementMethod.getInstance());

    }

    public boolean checkOnNext(HashMap<String, Boolean> condition) {
        if (condition.get(GlobalParameter.AccountStatus.isAllowLogin)) {
            if (condition.get(GlobalParameter.AccountStatus.isSetPassword)) {

                Intent intent = new Intent(this, CreatePasswordActivity.class);
                startActivity(intent);
                return true;
            }
            Intent intent = new Intent(this, InputPasswordActivity.class);
            Bundle bundle = new Bundle();
            Log.d("nikoo", " phone  :: " + TextUtil.getLocalPhone(etPhoneNumber.getText().toString()));
            bundle.putString(GlobalParameter.BundleParam.PARSING_PHONE,
                TextUtil.getLocalPhone(etPhoneNumber.getText().toString()));
            intent.putExtras(bundle);
            startActivity(intent);
            return true;
        }

        return false;
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @OnClick(R.id.btn_next)
    public void onClickNext() {
        showProgress("");
        loginPresenter.executeLogin(TextUtil.getLocalPhone(etPhoneNumber.getText().toString()));
    }


    @Override
    public void initInjection() {
        ((SelaiApplication) getApplication()).getApplicationComponent().Inject(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.clearAllSubscription();
    }


    @Override
    public void showError(String message) {

    }
}

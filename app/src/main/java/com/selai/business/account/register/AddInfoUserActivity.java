package com.selai.business.account.register;

import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.view.CustomSpinner;
import com.selai.business.view.TakepicProfile;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.BindView;

public class AddInfoUserActivity extends BaseActivity {

//    @BindView(R.id.tv_picture)
//    ImageView tvPicture;

    @BindView(R.id.tp_merchant)
    TakepicProfile tpMerchant;

    @BindView(R.id.et_name_store)
    EditText etNameStore;

    @BindView(R.id.et_address)
    EditText etAddress;

    @BindView(R.id.cs_province)
    CustomSpinner csProvince;

    @BindView(R.id.cs_city)
    CustomSpinner csCity;

    @BindView(R.id.cs_kecamatan)
    CustomSpinner csKecamatan;

    @BindView(R.id.cs_kelurahan)
    CustomSpinner csKelurahan;

    @BindView(R.id.et_postal_code)
    EditText etPostalCode;

    @BindView(R.id.tv_picture_person)
    ImageView tvPicturePerson;

    @BindView(R.id.et_name_person)
    EditText etNamePerson;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_no_ktp)
    EditText etNoKtp;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Override
    public int getLayout() {
        return R.layout.activity_add_info;
    }

    @Override
    public void setup() {

    }

    @Override
    public void initInjection() {

    }
}

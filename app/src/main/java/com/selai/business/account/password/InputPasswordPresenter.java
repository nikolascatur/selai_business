package com.selai.business.account.password;

import com.google.gson.Gson;

import com.selai.business.base.BasePresenter;
import com.selai.business.data.account.LoginEntity;
import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.network.response.LoginInquiryResponse;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class InputPasswordPresenter extends BasePresenter<InputPasswordContract.View> implements InputPasswordContract.Presenter {

    Context context;
    LoginEntity loginEntity;
    AccountRepository accountRepository;

    @Inject
    public InputPasswordPresenter(Context context,LoginEntity loginEntity){
        this.context = context;
        this.loginEntity = loginEntity;
        this.accountRepository = new AccountRepository(context,new Gson());

    }


    @Override
    public void executeNext(String phoneNumber, String password) {
        loginEntity.executePost(new DisposableObserver<Response<LoginInquiryResponse>>() {
            @Override
            public void onNext(Response<LoginInquiryResponse> loginInquiryResponseResponse) {
                if(loginInquiryResponseResponse.code() == 200){
                    accountRepository.saveProfile(loginInquiryResponseResponse.body().getData());
                    Log.d("nikoo"," getToken  "+loginInquiryResponseResponse.body().getData().getToken());
                    accountRepository.saveToken(loginInquiryResponseResponse.body().getData().getToken());
                    getView().onNextResult(
                        loginInquiryResponseResponse.body().getData().getToken(),
                        loginInquiryResponseResponse.body().getData().getMerchant(),
                        loginInquiryResponseResponse.body().getData().getOwner()
                    );
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        },phoneNumber, password);
    }

    @Override
    public void clearAllSubscription() {

    }
}

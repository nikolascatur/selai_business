package com.selai.business.account.password;

import com.selai.business.base.BaseContractView;
import com.selai.business.model.MerchantModel;
import com.selai.business.model.OwnerModel;

public interface InputPasswordContract {

    public interface View extends BaseContractView{
        public void onNextResult(String token,MerchantModel merchantModel,OwnerModel ownerModel);
    }

    public interface Presenter{

        public void executeNext(String phoneNumber,String password);

    }
}

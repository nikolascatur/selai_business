package com.selai.business.account.password;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.view.HideShowPassword;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

public class CreatePasswordActivity extends BaseActivity {

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.et_repassword)
    EditText etRepassword;

    @BindView(R.id.hsp_eye)
    HideShowPassword hspEye;

    @BindView(R.id.btn_next)
    Button btnNext;

    boolean isShowPassword = false;

    @Override
    public int getLayout() {
        return R.layout.activity_set_password;
    }

    @Override
    public void setup() {
        initObserverInput();
        hspEye.setEditTextLinking(etPassword,etRepassword);

    }

    public void initObserverInput() {
        Observable<Boolean> checker = RxTextView.textChanges(etRepassword)
            .map(CharSequence::toString).map(text -> text.equals(etPassword.getText()));
        addUiSubScription(checker.subscribe(enable -> btnNext.setEnabled(enable)));
    }


    @OnClick(R.id.btn_next)
    public void clickBtnNext() {

    }

    @Override
    public void initInjection() {

    }
}

package com.selai.business.account.password;

import com.google.gson.Gson;

import com.selai.business.R;
import com.selai.business.SelaiApplication;
import com.selai.business.base.BaseActivity;
import com.selai.business.dashboard.DashboardActivity;
import com.selai.business.data.account.LoginEntity;
import com.selai.business.data.account.repository.AccountRepository;
import com.selai.business.model.MerchantModel;
import com.selai.business.model.OwnerModel;
import com.selai.business.network.response.LoginInquiryResponse;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.Utils;
import com.selai.business.view.HideShowPassword;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class InputPasswordActivity extends BaseActivity implements InputPasswordContract.View {

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.hsp_eye)
    HideShowPassword hspEye;

    @BindView(R.id.btn_next)
    Button btnNext;

    String phoneNumber;

    @Inject
    InputPasswordPresenter inputPasswordPresenter;

    @Override
    public int getLayout() {
        return R.layout.activity_input_password;
    }

    @Override
    public void setup() {
        bindViewToPresenter();
        initShowHideEditText();
        bundleExtra();
    }

    public void bindViewToPresenter() {
        inputPasswordPresenter.setView(this);
    }

    public void bundleExtra() {
        Bundle bundle = getIntent().getExtras();
        phoneNumber = bundle.getString(GlobalParameter.BundleParam.PARSING_PHONE);
    }

    public void initShowHideEditText() {
        hspEye.setEditTextLinking(etPassword, null);
    }

    @OnClick(R.id.btn_next)
    public void nextActionButton() {
        showProgress("");
        inputPasswordPresenter.executeNext(phoneNumber, etPassword.getText().toString());
    }


    @Override
    public void initInjection() {
        ((SelaiApplication) getApplication()).getApplicationComponent().Inject(this);
    }

    @Override
    public void onNextResult(String token, MerchantModel merchantModel, OwnerModel ownerModel) {
        dismissProgress();
        Intent intent = new Intent(InputPasswordActivity.this, DashboardActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(GlobalParameter.BundleParam.TOKEN_PARSING, token);
        bundle.putParcelable(GlobalParameter.BundleParam.PARSING_MERCHANT, merchantModel);
        bundle.putParcelable(GlobalParameter.BundleParam.PARSING_OWNER, ownerModel);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }


    @Override
    public void showError(String message) {
        dismissProgress();
        Utils.ToastMessage(this,message,false);
    }
}

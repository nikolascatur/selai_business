package com.selai.business.account.login;

import com.selai.business.account.password.CreatePasswordActivity;
import com.selai.business.account.password.InputPasswordActivity;
import com.selai.business.base.BasePresenter;
import com.selai.business.data.account.PhoneEntity;
import com.selai.business.network.response.PhoneInquiryResponse;
import com.selai.business.util.GlobalParameter;
import com.selai.business.util.TextUtil;
import com.selai.business.util.Utils;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {

    PhoneEntity phoneEntity;

    @Inject
    public LoginPresenter(PhoneEntity phoneEntity){
        this.phoneEntity = phoneEntity;

    }

    @Override
    public void executeLogin(String phoneNumber) {
        phoneEntity.executePost(new DisposableObserver<Response<PhoneInquiryResponse>>() {
            @Override
            public void onNext(Response<PhoneInquiryResponse> phoneInquiryResponse) {
                if (phoneInquiryResponse.code() == 200 || phoneInquiryResponse.code() == 201) {
                    if (getView().checkOnNext(phoneInquiryResponse.body().getData()))
                        getView().closeActivity();
                    else {
                        Log.d("nikoo", "get valuee  :: " + phoneInquiryResponse.message());
                        String msgError = Utils.msgError(phoneInquiryResponse.errorBody());
                        getView().showError(msgError);
                    }
                }else{
                    String msgError = Utils.msgError(phoneInquiryResponse.errorBody());
                    getView().showError(msgError);
                }

            }

            @Override
            public void onError(Throwable e) {
                Log.d("nikoo", "error get Message " + e.getMessage());
                getView().showError(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        }, phoneNumber);
    }


    @Override
    public void clearAllSubscription() {
        phoneEntity.clearAllSubscription();
    }
}

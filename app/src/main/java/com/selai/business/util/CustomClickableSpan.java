package com.selai.business.util;

import android.content.Context;
import android.content.Intent;
import android.text.style.ClickableSpan;
import android.view.View;

public class CustomClickableSpan extends ClickableSpan {

    Context context;

    Intent intent;

    public CustomClickableSpan(Context context,Intent intent){
        this.context = context;
        this.intent = intent;
    }

    @Override
    public void onClick(View widget) {
        context.startActivity(intent);
    }
}

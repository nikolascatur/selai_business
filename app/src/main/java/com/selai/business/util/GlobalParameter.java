package com.selai.business.util;

import com.selai.business.BuildConfig;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;


public class GlobalParameter {

    @StringDef({
        AccountStatus.isAllowLogin,
        AccountStatus.isAllowRegister,
        AccountStatus.isNotActive,
        AccountStatus.isSetPassword
    }
    )


    public @interface AccountStatus {

        String isAllowLogin = "is_allow_login";

        String isAllowRegister = "is_allow_register";

        String isNotActive = "is_not_active";

        String isSetPassword = "is_set_password";

    }
    @IntDef({
        AccountIndex.allowLogin,
        AccountIndex.allowRegister,
        AccountIndex.notActive,
        AccountIndex.setPassword
    })
    public @interface AccountIndex {
        int allowLogin = 0;
        int allowRegister = 1;
        int notActive = 2;
        int setPassword = 3;
    }

    @StringDef({
        BundleParam.PARSING_PHONE,
        BundleParam.TOKEN_PARSING,
        BundleParam.PARSING_MERCHANT,
        BundleParam.PARSING_OWNER,
        BundleParam.PARSING_DATE,
        BundleParam.PARSING_ID_SALE,
        BundleParam.BUNDLE_CREATE_SALE
    })

    @IntDef({
        CallbackCode.RESULT_OK,
        CallbackCode.REQUEST_DATE
    })
    public @interface CallbackCode{
        int RESULT_OK = 200;
        int REQUEST_DATE = 100;
    }


    public @interface BundleParam{
        String PARSING_PHONE = "parsing_phone";
        String TOKEN_PARSING = "token_parsing";
        String PARSING_MERCHANT = "merchant_model";
        String PARSING_OWNER = "owner_model";

        String PARSING_DATE = "parsing_date";
        String PARSING_ID_SALE = "parsing_id_sale";

        String BUNDLE_CREATE_SALE = "bundle_create_sale";
    }

    @StringDef({
        PreferncesParam.ACCOUNT_PREFERENCES
    })
    public @interface PreferncesParam{
        String ACCOUNT_PREFERENCES = "AccountSelaiPrefernces"+ BuildConfig.FLAVOR;
    }
}

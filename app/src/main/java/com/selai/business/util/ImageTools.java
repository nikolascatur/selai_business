package com.selai.business.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ImageTools {

    private static final String FILE_SIZE = "File size : %1$s KB";

    private static final int MAX_QUALITY = 100;

    public static Bitmap pathToBitmap(String path) {
        File imagefile = new File(path);
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeStream(fileInputStream);
    }

    public static String bitmapToBase64(Bitmap bitmap) {
        byte[] byteArray = bitmapToByte(bitmap, MAX_QUALITY);
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static byte[] bitmapToByte(Bitmap bitmap, int imageQuality) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, imageQuality, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return byteArray;
    }

    public static Bitmap compress(Bitmap bitmap, int ImageQuality) {
        byte[] byteArray = bitmapToByte(bitmap, ImageQuality);
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxImageSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxImageSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxImageSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static Bitmap convert(Bitmap bitmap, Bitmap.Config config) {
        Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
        Canvas c = new Canvas();
        c.setBitmap(convertedBitmap);
        Paint p = new Paint();
        p.setFilterBitmap(true);
        c.drawBitmap(bitmap, 0, 0, p);
        bitmap.recycle();
        return convertedBitmap;
    }

    public static Bitmap getFixedFileSizeBitmap(String pathUri, int maxImageSize, int maxFileSize) {
        Bitmap fixedFileSizeBitmap = ImageTools.pathToBitmap(pathUri);
        fixedFileSizeBitmap = ImageTools.compress(fixedFileSizeBitmap, 50);
        fixedFileSizeBitmap = ImageTools.convert(fixedFileSizeBitmap, Bitmap.Config.RGB_565);
        do {
            fixedFileSizeBitmap = ImageTools
                .getResizedBitmap(fixedFileSizeBitmap, maxImageSize);
            maxImageSize = (int) (maxImageSize - (maxImageSize * 0.1));
        } while (getImageFileSize(fixedFileSizeBitmap) > maxFileSize);
        return fixedFileSizeBitmap;
    }

    public static int getImageFileSize(Bitmap bitmap) {
        long fileSize = bitmapToByte(bitmap, MAX_QUALITY).length / 1024;
        return (int) fileSize;
    }

}

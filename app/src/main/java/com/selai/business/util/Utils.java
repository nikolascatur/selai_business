package com.selai.business.util;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;

public class Utils {

    public static void ToastMessage(Context context,String message,boolean isLong){
        Toast.makeText(context,message,isLong?Toast.LENGTH_LONG:Toast.LENGTH_SHORT).show();
    }

    public static String msgError(ResponseBody responseBody){
//        ArrayList<String> listArr = new ArrayList<>();
//        listArr.add(getErrorMessage(responseBody).get("messages").toString());
//        Log.d("nikoo"," CCCCCC length text :: "+listArr.size()+" === "+listArr.get(0));
        return getErrorMessage(responseBody).get("messages").toString();
    }

    public static Map<String, Object> getErrorMessage(ResponseBody responseBody) {
        Map<String, Object> result = new HashMap<>();
        try {
            if (responseBody != null) {
                String base = responseBody.string();
                JSONObject json = new JSONObject(base);
                if (json != null) {
                    Iterator<String> key = json.keys();
                    while (key.hasNext()) {
                        String keyData = key.next();
                        Object objectData = json.get(keyData);
                        if (objectData instanceof JSONObject) {
                            JSONObject jsonObjectData = (JSONObject) objectData;
                            Iterator<String> keyObject = jsonObjectData.keys();
                            while (keyObject.hasNext()) {
                                String resultKey = keyObject.next();
                                Object keyValue = jsonObjectData.get(resultKey);
                                Log.d("nikoo","BBBB :: type "+keyValue.getClass()+"  result key "+resultKey);
                                result.put(resultKey, keyValue);
                            }
                        }
                        else{
                            Log.d("nikoo","AAA object data type  "+objectData.getClass()+" key data "+keyData);
                            result.put(keyData,objectData);
                        }
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Date StringtoDate(String dt){
        Date date = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
        try {
            date = simpleDateFormat.parse(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

   public static String changeFormatString(String dt){
        Date date = StringtoDate(dt);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyy, hh:mm");
        return simpleDateFormat.format(date);
   }

    public static String getCurrencyFormat(long total) {
        String currency = "";
        try {
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMinimumFractionDigits(0);
            nf.setMaximumFractionDigits(0);
            currency = "" + nf.format(total);
        } catch (Exception e) {
        }
        return currency;
    }

}

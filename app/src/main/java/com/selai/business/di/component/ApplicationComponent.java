package com.selai.business.di.component;


import com.selai.business.account.login.LoginActivity;
import com.selai.business.account.password.InputPasswordActivity;
import com.selai.business.base.BaseActivity;
import com.selai.business.checkout.CheckoutActivity;
import com.selai.business.dashboard.DashboardActivity;
import com.selai.business.di.module.ApplicationModule;
import com.selai.business.di.module.NetModule;
import com.selai.business.history.SaleHistoryActivity;
import com.selai.business.profile.ProfileActivity;
import com.selai.business.setting.SettingActivity;
import com.selai.business.splash.SplashActivity;
import com.selai.business.transaction.additem.AddItemActivity;
import com.selai.business.transaction.create.CreateFlashSaleActivity;
import com.selai.business.transaction.productlist.ProductListActivity;
import com.selai.business.transaction.schedule.ScheduleActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by nikolascatur on 22/01/18.
 */
@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface ApplicationComponent {

    void Inject(BaseActivity baseActivity);

    void Inject(LoginActivity loginActivity);

    void Inject(InputPasswordActivity inputPasswordActivity);

    void Inject(DashboardActivity dashboardActivity);

    void Inject(CreateFlashSaleActivity createFlashSaleActivity);

    void Inject(SplashActivity splashActivity);

    void Inject(ScheduleActivity scheduleActivity);

    void Inject(AddItemActivity addItemActivity);

    void Inject(ProductListActivity productListActivity);

    void Inject(SaleHistoryActivity saleHistoryActivity);

    void Inject(SettingActivity settingActivity);

    void Inject(ProfileActivity profileActivity);

    void Inject(CheckoutActivity checkoutActivity);
}

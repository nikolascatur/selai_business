package com.selai.business.di.module;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by nikolascatur on 23/01/18.
 */
@Module
public class FirebaseModule {

    @Provides
    @Singleton
    FirebaseAuth provideFirebaseAuth(){
        return FirebaseAuth.getInstance();
    }

    @Provides
    @Singleton
    FirebaseDatabase providesFirebaseDatabase(){
        FirebaseDatabase fire = FirebaseDatabase.getInstance();
        fire.setPersistenceEnabled(false);
        return fire;
    }



}

package com.selai.business.setting;

import com.selai.business.base.BaseContractView;

public interface SettingContract {

    interface View extends BaseContractView{

    }

    interface Presenter{

    }
}

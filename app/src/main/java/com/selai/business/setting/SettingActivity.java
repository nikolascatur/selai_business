package com.selai.business.setting;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.model.InfoSetting;
import com.selai.business.setting.adapter.InfoAdapter;
import com.selai.business.setting.adapter.NotifToggleAdapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

public class SettingActivity extends BaseActivity implements SettingContract.View{

    @BindView(R.id.civ_profile_pic)
    CircularImageView civProfilePic;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_phone_number)
    TextView tvPhoneNumber;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.rv_info)
    RecyclerView rvInfo;
    InfoAdapter infoAdapter;
    List<InfoSetting> datalist;

    @BindView(R.id.rv_sosmed)
    RecyclerView rvSosmed;
    List<InfoSetting> sosMedList;
    InfoAdapter sosmedAdapter;

    @BindView(R.id.rv_userVerified)
    RecyclerView rvUserVerified;
    List<InfoSetting> userVreified;
    InfoAdapter userVerAdapter;

    @BindView(R.id.title_notif)
    ConstraintLayout titleNotif;
    @BindView(R.id.rv_notif)
    RecyclerView rvNotif;
    List<InfoSetting> notifList;
    NotifToggleAdapter notifAdapter;

    @BindView(R.id.rv_tnc)
    RecyclerView rvTnc;
    List<InfoSetting> tncList;
    InfoAdapter tncAdapter;

    @BindView(R.id.logout_info)
    ConstraintLayout logoutInfo;


    private final static InfoSetting infoData[] = {
        new InfoSetting(R.drawable.ic_phone,"Nomor Handphone","085643138359","",InfoSetting.TYPE_INFO_SETTING.PHONE,true),
        new InfoSetting(R.drawable.ic_envelope,"Alamat Email","Sarah@gm.vom","",InfoSetting.TYPE_INFO_SETTING.ADDRESS,true),
        new InfoSetting(R.drawable.ic_key,"Ganti Password","","",InfoSetting.TYPE_INFO_SETTING.PASSWORD,true),
        new InfoSetting(R.drawable.ic_language,"Bahasa","","Bahasa Indonesia",InfoSetting.TYPE_INFO_SETTING.LANGUAGE,true),
    };

    private final static InfoSetting infoSosmed[] = {
        new InfoSetting(R.drawable.ic_facebook,"Facebook","","Terhubung",
            InfoSetting.TYPE_INFO_SETTING.FACEBOOK,false),
        new InfoSetting(R.drawable.ic_gplus,"Google","","Belum Terhubung",
            InfoSetting.TYPE_INFO_SETTING.GPLUS,false),
    };

    private final static InfoSetting infoVerification[] = {
        new InfoSetting(R.drawable.ic_verification,"Verifikasi Pengguna","","Verified",
            InfoSetting.TYPE_INFO_SETTING.USER_VERIFICATION,false),
        new InfoSetting(R.drawable.ic_rekening,"Rekening Bank","","",
            InfoSetting.TYPE_INFO_SETTING.USER_VERIFICATION,false)
    };

    private final static InfoSetting notifSetting[] = {
        new InfoSetting(0,"Buletin","Setiap promosi dan informasi update seputar Flash Sale","", InfoSetting.TYPE_INFO_SETTING.USER_VERIFICATION,true),
        new InfoSetting(0,"Pengingat Flash Sale","Setiap flash sale yang akan dimulai","", InfoSetting.TYPE_INFO_SETTING.USER_VERIFICATION,true),
        new InfoSetting(0,"Ada Stok Flash Sale","Setiap flash sale yang diikuti dan masih stok masih tersedia","", InfoSetting.TYPE_INFO_SETTING.USER_VERIFICATION,true),
        new InfoSetting(0,"Perubahan Status Order","Setiap perubahan status atas produk yang dipesan","", InfoSetting.TYPE_INFO_SETTING.USER_VERIFICATION,true),
        new InfoSetting(0,"Pesan Pribadi","Setiap Pesan Pribadi yang saya terima","", InfoSetting.TYPE_INFO_SETTING.USER_VERIFICATION,true)
    };

    private final static InfoSetting tncSetting[] = {
        new InfoSetting(R.drawable.ic_ask,"Pertanyaan yang sering diajukan","","", InfoSetting.TYPE_INFO_SETTING.QUESTION,true),
        new InfoSetting(R.drawable.ic_tnc,"Syarat dan ketentuan","","", InfoSetting.TYPE_INFO_SETTING.TNC,true),
        new InfoSetting(R.drawable.ic_privacypolic,"Kebijakan pribadi","","", InfoSetting.TYPE_INFO_SETTING.PRIVACY_POLICY,true)
    };

    public void initTnCSetting(){
        tncList = Arrays.asList(tncSetting);
        tncAdapter = new InfoAdapter(tncList) {
            @Override
            public void clickAction(InfoSetting.TYPE_INFO_SETTING enumAction) {
                switch (enumAction){
                    case QUESTION:

                        break;
                    case TNC:

                        break;
                    case PRIVACY_POLICY:

                        break;
                }
            }
        };

        rvTnc.setLayoutManager(new LinearLayoutManager(this));
        rvTnc.setAdapter(tncAdapter);
    }

    public void setTextConstrain(ConstraintLayout cons,int icon,String title){
        ImageView ivIcon = cons.findViewById(R.id.iv_icon);
        TextView tvTitle = cons.findViewById(R.id.tv_title);
        ivIcon.setImageResource(icon);
        tvTitle.setText(title);
    }

    public void initNotifSetting(){
        notifList = Arrays.asList(notifSetting);
        notifAdapter = new NotifToggleAdapter(notifList);
        rvNotif.setLayoutManager(new LinearLayoutManager(this));
        rvNotif.setAdapter(notifAdapter);
    }

    public void initInfo(){
        datalist = Arrays.asList(infoData);
        infoAdapter = new InfoAdapter(datalist) {
            @Override
            public void clickAction(InfoSetting.TYPE_INFO_SETTING enumAction) {
                switch (enumAction){
                    case PHONE:

                        break;
                    case ADDRESS:

                        break;
                    case PASSWORD:

                        break;
                    case LANGUAGE:

                        break;

                }
            }
        };
        rvInfo.setLayoutManager(new LinearLayoutManager(this));
        rvInfo.setAdapter(infoAdapter);
    }

    public void initSosmed(){
        sosMedList = Arrays.asList(infoSosmed);
        sosmedAdapter = new InfoAdapter(sosMedList) {
            @Override
            public void clickAction(InfoSetting.TYPE_INFO_SETTING enumAction) {
                switch (enumAction){
                    case FACEBOOK:

                        break;
                    case GPLUS:

                        break;
                }
            }
        };

        rvSosmed.setLayoutManager(new LinearLayoutManager(this));
        rvSosmed.setAdapter(sosmedAdapter);
    }

    public void initVerifiedUser(){
        userVreified= Arrays.asList(infoVerification);
        userVerAdapter = new InfoAdapter(userVreified) {
            @Override
            public void clickAction(InfoSetting.TYPE_INFO_SETTING enumAction) {

            }
        };
        rvUserVerified.setLayoutManager(new LinearLayoutManager(this));
        rvUserVerified.setAdapter(userVerAdapter);

    }

    @Override
    public int getLayout() {
        return R.layout.activity_setting;
    }

    @Override
    public void setup() {
        initInfo();
        changeInfoPhone("085643138359");
        changeInfoEmail("nikolascatur@gmail.com");
        changeInfoLanguage("Bahasa Jawa");

        initSosmed();

        initVerifiedUser();

        setTextConstrain(titleNotif,R.drawable.ic_notification,"Notifikasi");
        initNotifSetting();

        initTnCSetting();
        setTextConstrain(logoutInfo,R.drawable.ic_logout,"Logout");
    }

    public void changeInfoPhone(String noPhone){
        datalist.get(0).setInfo(noPhone);
        infoAdapter.notifyDataSetChanged();
    }

    public void changeInfoEmail(String email){
        datalist.get(1).setInfo(email);
        infoAdapter.notifyDataSetChanged();
    }

    public void changeInfoLanguage(String email){
        datalist.get(3).setInfo_horizontal(email);
        infoAdapter.notifyDataSetChanged();
    }


    @Override
    public void initInjection() {
        getApplicationComponent().Inject(this);
    }

    @Override
    public void showError(String message) {

    }
}

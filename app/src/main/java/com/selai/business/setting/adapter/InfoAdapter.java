package com.selai.business.setting.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.InfoSetting;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public abstract class InfoAdapter extends BaseQuickAdapter<InfoSetting, BaseViewHolder> {

    public InfoAdapter(@Nullable List<InfoSetting> data) {
        super(R.layout.item_info, data);
    }

    public InfoAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, InfoSetting item) {
        ((ImageView) helper.getView(R.id.iv_icon)).setImageResource(item.getIcon());
        helper.setText(R.id.tv_title, item.getTitle());

        if (item.getInfo().equals(""))
            helper.getView(R.id.tv_info).setVisibility(View.GONE);

        helper.setText(R.id.tv_info, item.getInfo());


        if (item.getInfo_horizontal().equals(""))
            helper.getView(R.id.tv_info_horizontal).setVisibility(View.GONE);

        helper.setText(R.id.tv_info_horizontal, item.getInfo_horizontal());

        TextView tvTitle = helper.getView(R.id.tv_title);
        ImageView ivImage = helper.getView(R.id.iv_actiont);

        tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAction(item.getAction());
            }
        });

        ivImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickAction(item.getAction());
            }
        });
    }

    public abstract void clickAction(InfoSetting.TYPE_INFO_SETTING enumAction);
}

package com.selai.business.setting.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.InfoSetting;

import android.support.annotation.Nullable;
import android.widget.Switch;

import java.util.List;

public class NotifToggleAdapter extends BaseQuickAdapter<InfoSetting,BaseViewHolder> {

    public NotifToggleAdapter(
        @Nullable List<InfoSetting> data) {
        super(R.layout.item_setting_notif, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InfoSetting item) {
        helper.setText(R.id.tv_title,item.getTitle()).setText(R.id.tv_info,item.getInfo());

        Switch ivSwitch = helper.getView(R.id.sw_toggle);
        ivSwitch.setChecked(item.isArrShow());
    }
}

package com.selai.business.setting;

import com.selai.business.base.BasePresenter;

import javax.inject.Inject;

public class SettingPresenter extends BasePresenter<SettingContract.View> implements SettingContract.Presenter {


    @Inject
    public SettingPresenter(){

    }

    @Override
    public void clearAllSubscription() {

    }
}

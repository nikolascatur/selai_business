package com.selai.business.network.response;

import com.selai.business.model.ProductItem;
import com.selai.business.network.BaseResponse;

import java.util.List;

public class ProductListResponse extends BaseResponse {

    private List<ProductItem> data;


    public List<ProductItem> getData() {
        return data;
    }
}

package com.selai.business.network.response;

import com.selai.business.network.BaseResponse;

import java.util.HashMap;

public class PhoneInquiryResponse extends BaseResponse {

    private HashMap<String,Boolean> data;

    public HashMap<String, Boolean> getData() {
        return data;
    }

}

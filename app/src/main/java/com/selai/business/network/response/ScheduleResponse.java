package com.selai.business.network.response;

import com.google.gson.annotations.SerializedName;

import com.selai.business.network.BaseResponse;

import java.util.HashMap;

public class ScheduleResponse extends BaseResponse {

    @SerializedName("data")
    private HashMap<String, HashMap<String, Boolean>> data;

    public HashMap<String, HashMap<String, Boolean>> getData() {
        return data;
    }
}

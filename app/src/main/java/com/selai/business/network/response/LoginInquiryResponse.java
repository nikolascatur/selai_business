package com.selai.business.network.response;

import com.google.gson.annotations.SerializedName;

import com.selai.business.model.MerchantModel;
import com.selai.business.model.OwnerModel;
import com.selai.business.network.BaseResponse;

public class LoginInquiryResponse extends BaseResponse {

    private LoginData data;

    public LoginData getData() {
        return data;
    }

    public class LoginData {

        private MerchantModel merchant;

        private OwnerModel owner;

        @SerializedName("token")
        private String token;

        public MerchantModel getMerchant() {
            return merchant;
        }

        public OwnerModel getOwner() {
            return owner;
        }

        public String getToken() {
            return token;
        }
    }

}

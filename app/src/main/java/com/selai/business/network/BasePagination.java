package com.selai.business.network;

import com.google.gson.annotations.SerializedName;

public class BasePagination {

    @SerializedName("page")
    private int page;

    @SerializedName("per_page")
    private int per_page;

    @SerializedName("total_page")
    private int total_page;

    @SerializedName("total_data")
    private int total_data;

    public int getPage() {
        return page;
    }

    public int getPer_page() {
        return per_page;
    }

    public int getTotal_page() {
        return total_page;
    }

    public int getTotal_data() {
        return total_data;
    }
}

package com.selai.business.network.base;

/**
 * Created by nikolascatur on 20/02/18.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BaseNetwork<T> {
    public static final String DATE_FORMAT = "yyyy-MM-dd\'T\'hh:mm:ssZ";
    private T networkService;

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public BaseNetwork() {
    }

    protected void initNetworkInterface() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(Level.NONE);

        OkHttpClient client = this.okHttpClientHandler(new Builder()).addInterceptor(loggingInterceptor).build();


        Gson gson = this.gsonHandler((new GsonBuilder()).setPrettyPrinting()).setDateFormat("yyyy-MM-dd\'T\'hh:mm:ssZ").create();
        retrofit2.Retrofit.Builder retrofitBuilder = (new retrofit2.Retrofit.Builder()).baseUrl(this.getBaseUrl())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        Retrofit retrofit = this.retrofitHandler(retrofitBuilder).addConverterFactory(
            GsonConverterFactory.create(gson)).client(client).build();
        RetrofitHelper.init(retrofit);
        this.networkService = retrofit.create(this.getRestClass());
    }

    public T getNetworkService() {
        if(this.networkService == null) {
            this.initNetworkInterface();
        }

        return this.networkService;
    }

    protected Builder okHttpClientHandler(Builder builder) {
        return builder;
    }

    protected retrofit2.Retrofit.Builder retrofitHandler(retrofit2.Retrofit.Builder builder) {
        return builder;
    }

    protected GsonBuilder gsonHandler(GsonBuilder builder) {
        return builder;
    }

    protected abstract String getBaseUrl();

    protected abstract Class<T> getRestClass();

    public void clearAllSubscription(){
        compositeDisposable.clear();
    }

    public Disposable addSubscription(Disposable disposable){
        compositeDisposable.add(disposable);
        return compositeDisposable;
    }
}

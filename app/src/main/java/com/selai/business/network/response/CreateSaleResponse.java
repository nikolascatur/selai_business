package com.selai.business.network.response;

import com.google.gson.annotations.SerializedName;

import com.selai.business.model.CourierModel;
import com.selai.business.model.CreateSaleModel;
import com.selai.business.network.BaseResponse;

import java.util.List;

public class CreateSaleResponse extends BaseResponse {

    private CreateSaleModel data;

    public CreateSaleModel getData() {
        return data;
    }
}

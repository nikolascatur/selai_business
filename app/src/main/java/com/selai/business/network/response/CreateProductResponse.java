package com.selai.business.network.response;

import com.google.gson.annotations.SerializedName;

import com.selai.business.model.ProductItem;
import com.selai.business.model.Variant;
import com.selai.business.network.BaseResponse;

import java.util.List;

public class CreateProductResponse extends BaseResponse {

    private ProductItem data;

    public ProductItem getData() {
        return data;
    }

}

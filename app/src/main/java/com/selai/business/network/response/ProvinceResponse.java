package com.selai.business.network.response;

import com.selai.business.model.Province;
import com.selai.business.network.BaseResponse;

import java.util.List;

public class ProvinceResponse extends BaseResponse {

    private List<Province> data;

    public List<Province> getData() {
        return data;
    }
}

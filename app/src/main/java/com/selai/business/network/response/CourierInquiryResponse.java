package com.selai.business.network.response;

import com.selai.business.model.CourierModel;
import com.selai.business.network.BaseResponse;

import java.util.List;

public class CourierInquiryResponse extends BaseResponse {

    private List<CourierModel> data;

    public List<CourierModel> getData() {
        return data;
    }



}

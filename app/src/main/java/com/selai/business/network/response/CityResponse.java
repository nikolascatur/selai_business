package com.selai.business.network.response;

import com.selai.business.model.City;
import com.selai.business.network.BaseResponse;

import java.util.List;

public class CityResponse extends BaseResponse {
    private List<City> data;

    public List<City> getData() {
        return data;
    }
}

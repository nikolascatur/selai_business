package com.selai.business.network.base;

/**
 * Created by nikolascatur on 20/02/18.
 */

import retrofit2.Retrofit;

public class RetrofitHelper {
    private static Retrofit retrofit;

    public RetrofitHelper() {
    }

    public static void init(Retrofit retrofit) {
        RetrofitHelper.retrofit = retrofit;
    }

    public static Retrofit retrofit() {
        return retrofit;
    }
}

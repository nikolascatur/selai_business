package com.selai.business.network.response;

import com.selai.business.model.CreateSaleModel;
import com.selai.business.network.BasePagination;
import com.selai.business.network.BaseResponse;

import java.util.List;

public class FlashSaleResponse extends BaseResponse {

    private List<CreateSaleModel> data;

    public List<CreateSaleModel> getData() {
        return data;
    }

//    public class FlashSaleListData extends BasePagination{
//        private List<CreateSaleModel> list;
//
//        public List<CreateSaleModel> getList() {
//            return list;
//        }
//    }
}

package com.selai.business.network.response;

import com.selai.business.network.BaseResponse;

import java.util.HashMap;

public class ProfileInquiryResponse extends BaseResponse {

    private HashMap<String,Object> data;

    public HashMap<String, Object> getData() {
        return data;
    }
}

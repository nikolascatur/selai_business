package com.selai.business.profile;

import com.selai.business.base.BasePresenter;

import javax.inject.Inject;

public class ProfilePresenter extends BasePresenter<ProfileContract.View> implements ProfileContract.Presenter {


    @Inject
    public ProfilePresenter(){

    }

    @Override
    public void clearAllSubscription() {

    }
}

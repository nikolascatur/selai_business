package com.selai.business.profile.adapter;

import com.bumptech.glide.load.engine.Resource;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.selai.business.R;
import com.selai.business.model.SaleStatistic;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class ProfileAdapter extends BaseQuickAdapter<SaleStatistic,BaseViewHolder> {

    public ProfileAdapter(
        @Nullable List<SaleStatistic> data) {
        super(R.layout.item_sale_statistic, data);
    }

    public ProfileAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, SaleStatistic item) {
        LinearLayout llStatisctic =  helper.getView(R.id.ll_statistic);
        ImageView ivLogo = helper.getView(R.id.logo);
        TextView tvCount = helper.getView(R.id.tv_count);
        TextView tvDesc = helper.getView(R.id.tv_description);
        switch (item.getType()){
            case JUST_TEXT:
                llStatisctic.setBackgroundColor(Color.WHITE);
                ivLogo.setVisibility(View.GONE);
                tvCount.setVisibility(View.GONE);
                tvDesc.setText(item.getDescription());
                break;
            case ICON:
                llStatisctic.setBackground(ContextCompat.getDrawable(mContext,R.drawable.blue_rounded_8));
                ivLogo.setVisibility(View.VISIBLE);
                tvCount.setVisibility(View.GONE);
                tvCount.setText(item.getTotal());
                tvDesc.setText(item.getDescription());
                break;
            case REGULAR:
                llStatisctic.setBackground(ContextCompat.getDrawable(mContext,R.drawable.grey_rounded_8));
                ivLogo.setVisibility(View.GONE);
                tvCount.setVisibility(View.VISIBLE);
                tvDesc.setText(item.getDescription());
                tvDesc.setTextColor(ContextCompat.getColor(mContext,R.color.grey_dark_light));
                break;
        }
    }
}

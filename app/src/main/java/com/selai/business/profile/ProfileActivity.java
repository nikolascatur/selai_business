package com.selai.business.profile;

import com.selai.business.R;
import com.selai.business.base.BaseActivity;
import com.selai.business.checkout.CheckoutActivity;
import com.selai.business.model.SaleStatistic;
import com.selai.business.profile.adapter.ProfileAdapter;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ProfileActivity extends BaseActivity implements ProfileContract.View {

    @Inject
    ProfilePresenter profilePresenter;

    List<SaleStatistic> listStatistic;
    ProfileAdapter profileAdapter;

    @BindView(R.id.rv_statistic)
    RecyclerView rvStatistic;

    @BindView(R.id.toolbar)
    View toolbar;

    @Override
    public int getLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public void setup() {
        bindViewToPresenter();
        configToolbar(toolbar,getString(R.string.profile));
        initAdapter();
    }

    public void initAdapter(){
        notEmpty();
        profileAdapter = new ProfileAdapter(listStatistic);
        rvStatistic.setLayoutManager(new GridLayoutManager(this,2));
        rvStatistic.setAdapter(profileAdapter);
    }

    public void emptySale(){
        listStatistic = new ArrayList<>();
        listStatistic.add(new SaleStatistic("",getString(R.string.sale_statistic), SaleStatistic.TYPE_BUTTON.JUST_TEXT));
        listStatistic.add(new SaleStatistic("",getString(R.string.crate_flashsale), SaleStatistic.TYPE_BUTTON.ICON));
    }

    public void notEmpty(){
        listStatistic = new ArrayList<>();
        listStatistic.add(new SaleStatistic("45",getString(R.string.desc_sale_month), SaleStatistic.TYPE_BUTTON.REGULAR));
        listStatistic.add(new SaleStatistic("50",getString(R.string.desc_sale_total), SaleStatistic.TYPE_BUTTON.REGULAR));
        listStatistic.add(new SaleStatistic("756",getString(R.string.sale_created), SaleStatistic.TYPE_BUTTON.REGULAR));
        listStatistic.add(new SaleStatistic("",getString(R.string.crate_flashsale), SaleStatistic.TYPE_BUTTON.ICON));
    }

    public void bindViewToPresenter(){
        profilePresenter.setView(this);
    }

    @OnClick(R.id.tv_saldo_checkout)
    public void clickSaldoCheckout(){
        Intent intent = new Intent(this, CheckoutActivity.class);
        startActivity(intent);
    }

    @Override
    public void initInjection() {
        getApplicationComponent().Inject(this);
    }

}
